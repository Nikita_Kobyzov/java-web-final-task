package by.epam.task6.filter;

import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * <p>This class is used to check access to the command</p>
 * @author Nikita Kobyzov
 */
public class AccessFilter implements Filter {

    /**
     * <p>This field is used as a key to receive a command name from request</p>
     */
    private static final String ACTION_KEY = "ACTION";

    /**
     * <p>This field is used as a key to get User-object from HttpSession</p>
     */
    private static final String USER_KEY = "user";

    /**
     * <p>This field is used as a default page is the request is not valid</p>
     */
    private static final String DEFAULT_PAGE = "index.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fChain)
            throws IOException, ServletException {
        boolean isValid;
        String commandName = request.getParameter(ACTION_KEY);
        if (commandName == null) {
            isValid = false;
        } else {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            UserStatus userStatus = getUserStatus(httpRequest);
            isValid = isValid(commandName, userStatus);
        }
        if (!isValid) {
            request.getRequestDispatcher(DEFAULT_PAGE).forward(request, response);
            return;
        }
        fChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    /**
     * <p>This enum consists the names of admin commands</p>
     */
    public enum AdminCommandName {
        DELETE_BOOK, ADD_BOOK, BOOK_EDITOR, EDIT_BOOK, CONFIRM_ORDER, DELETE_ORDER,
        ADMIN_ORDER_LIST
    }

    /**
     * <p>This enum consists the names of client commands</p>
     */
    public enum ClientCommandName {
        USER_CART, TAKE_BOOK, REMOVE_BOOK_FROM_CART, ORDER_LIST, TO_CART
    }

    /**
     * <p>This method checks access to the command</p>
     *
     * @param commandName is a name of the command
     * @param userStatus  is a status of the current user
     * @return <tt>true</tt> if the user has access to the command
     */
    private boolean isValid(String commandName, UserStatus userStatus) {
        AdminCommandName[] adminCommandNames = AdminCommandName.values();
        for (AdminCommandName command : adminCommandNames) {
            if (command.name().equals(commandName)) {
                if (userStatus.equals(UserStatus.ADMIN)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        ClientCommandName[] userCommandNames = ClientCommandName.values();
        for (ClientCommandName command : userCommandNames) {
            if (command.name().equals(commandName)) {
                if (userStatus.equals(UserStatus.CLIENT)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <p>This method returns a status of the current user</p>
     *
     * @param request is request sent from the client
     * @return a status of the current user
     */
    private UserStatus getUserStatus(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return UserStatus.NONE;
        }
        if (session.getAttribute(USER_KEY) == null) {
            return UserStatus.NONE;
        }
        User user = (User) session.getAttribute(USER_KEY);
        return user.getUserStatus();
    }
}
