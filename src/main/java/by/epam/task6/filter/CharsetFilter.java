package by.epam.task6.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * <p>This class is used as a filter to set a UTF-8 character encoding to the
 * request and response</p>
 * @author Nikita Kobyzov
 */
public class CharsetFilter implements Filter {
    private static final String UTF_8_ENCODING_KEY = "UTF-8";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fChain)
            throws IOException, ServletException {
        request.setCharacterEncoding(UTF_8_ENCODING_KEY);
        response.setCharacterEncoding(UTF_8_ENCODING_KEY);
        fChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
