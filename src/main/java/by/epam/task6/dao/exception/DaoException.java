package by.epam.task6.dao.exception;

/**
 * <p>This exception throws when the error occurred in working with the data source</p>
 * @author Nikita Kobyzov
 */
public class DaoException extends Exception {

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }
}
