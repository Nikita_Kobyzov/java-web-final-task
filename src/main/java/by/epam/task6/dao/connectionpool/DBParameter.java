package by.epam.task6.dao.connectionpool;

/**
 * <p>This class is used to obtain the keys to read the property file</p>
 * @author Nikita Kobyzov
 */
public class DBParameter {
    public static final String SIZE_KEY = "db.size";
    public static final String DRIVER_KEY = "db.driver";
    public static final String URL_KEY = "db.url";
    public static final String USER_KEY = "db.user";
    public static final String PASSWORD_KEY = "db.password";
}
