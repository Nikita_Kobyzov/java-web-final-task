package by.epam.task6.dao.connectionpool.impl;

import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.DBParameter;
import by.epam.task6.dao.connectionpool.IConnectionPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * <p>This class contains the database connection. It implements the interface
 * {@link by.epam.task6.dao.connectionpool.IConnectionPool}</p>
 * @author Nikita Kobyzov
 */
public class ConnectionPool implements IConnectionPool {
    private static final Logger LOG = LogManager.getLogger(ConnectionPool.class);
    private int size;
    private String url;
    private String driver;
    private String user;
    private String password;

    /**
     * <p>This queue contains all free connections</p>
     */
    private BlockingQueue<Connection> connections;

    /**
     * <p>This list contains all connections</p>
     */
    private static final List<Connection> listConnection = new ArrayList<Connection>();
    private static final ConnectionPool INSTANCE = new ConnectionPool();

    private ConnectionPool() {
    }

    public void init() throws ConnectionPoolException {
        String resourceName = "config.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            props.load(resourceStream);
        } catch (IOException e) {
            LOG.error("Error in connection pool initialize", e);
            throw new ConnectionPoolException("Error in connection pool initialize", e);
        }
        size = Integer.parseInt(props.getProperty(DBParameter.SIZE_KEY));
        url = props.getProperty(DBParameter.URL_KEY);
        driver = props.getProperty(DBParameter.DRIVER_KEY);
        password = props.getProperty(DBParameter.PASSWORD_KEY);
        user = props.getProperty(DBParameter.USER_KEY);
        try {
            Class.forName(driver);
            connections = new ArrayBlockingQueue<Connection>(size);
            for (int i = 0; i < size; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection pConnection = new PooledConnection(connection, this);
                connections.add(pConnection);
                listConnection.add(pConnection);
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new ConnectionPoolException("Unable to initialize the Connection pool", e);
        }

    }

    public void init(String url, String driver, String user, String password, int size)
            throws ConnectionPoolException {
        try {
            this.size = size;
            this.url = url;
            this.driver = driver;
            this.user = user;
            this.password = password;

            Class.forName(driver);
            connections = new ArrayBlockingQueue<Connection>(size);
            for (int i = 0; i < size; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection pConnection = new PooledConnection(connection, this);
                connections.add(pConnection);
                listConnection.add(pConnection);
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new ConnectionPoolException("Unable to initialize the Connection pool", e);
        }


    }

    public void destroy() throws ConnectionPoolException {
        SQLException exception = null;
        for (Connection connection : listConnection) {
            try {
                if (connection != null) {
                    if (!connection.getAutoCommit()) {
                        connection.commit();
                    }
                    ((PooledConnection) connection).dispose();
                }
            } catch (SQLException e) {
                exception = e;
            }
        }
        if (exception != null) {
            throw new ConnectionPoolException("Can not close the connection");
        }

    }

    public static ConnectionPool getInstance() {
        return INSTANCE;
    }

    public Connection getConnection() throws ConnectionPoolException {
        try {
            Connection connection = connections.poll(10, TimeUnit.SECONDS);
            if (connection == null) {
                throw new ConnectionPoolException("Unable to get a connection");
            }
            return connection;
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e.getMessage());
        }

    }

    /**
     * <p>This method put back the connection to the queue</p>
     *
     * @param connection is the object that will be put to the queue
     */
    private void putConnection(Connection connection) {
        connections.offer(connection);
    }

    /**
     * <p>This class implements interface {@link java.sql.Connection}. This class was
     * created to override a method of closing to prevent a real connection closing</p>
     */
    private class PooledConnection implements Connection {
        /**
         * <p>The connection that was given by database driver</p>
         */
        private Connection connection;
        private ConnectionPool connectionPool;

        public PooledConnection(Connection connection, ConnectionPool connectionPool)
                throws SQLException {
            this.connection = connection;
            this.connection.setAutoCommit(true);
            this.connectionPool = connectionPool;
        }

        /**
         * <p>This method really close the connection</p>
         *
         * @throws SQLException
         */
        public void dispose() throws SQLException {
            this.connection.close();
        }

        @Override
        public void close() throws SQLException {
            connection.setAutoCommit(true);
            if (connection.isReadOnly()) {
                connection.setReadOnly(false);
            }
            connectionPool.putConnection(this);
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        @Override
        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        @Override
        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void commit() throws SQLException {
            connection.commit();
        }

        @Override
        public void rollback() throws SQLException {
            connection.rollback();
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback(savepoint);
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        @Override
        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }

        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        @Override
        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName, attributes);
        }

        @Override
        public void setSchema(String schema) throws SQLException {
            connection.setSchema(schema);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
            connection.setNetworkTimeout(executor, milliseconds);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }
    }
}
