package by.epam.task6.dao.connectionpool.exception;

/**
 * <p>This exception throws when the error occurred in working with the
 * connection pool</p>
 * @author Nikita Kobyzov
 */
public class ConnectionPoolException extends Exception {

    public ConnectionPoolException(String message) {
        super(message);
    }


    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
