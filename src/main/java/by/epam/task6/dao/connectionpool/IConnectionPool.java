package by.epam.task6.dao.connectionpool;

import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;

import java.sql.Connection;

/**
 * <p>This interface describes the basic methods for the connection pool</p>
 * @see by.epam.task6.dao.connectionpool.impl.ConnectionPool
 * @author Nikita Kobyzov
 */
public interface IConnectionPool {
    /**
     * <p>This method initialize the connection pool. The database config reads from
     * the property file</p>
     *
     * @throws ConnectionPoolException if the error occurred in working with the
     *                                 connection pool
     */
    void init() throws ConnectionPoolException;

    /**
     * <p>This method initialize the connection pool</p>
     *
     * @param url      is the path to database
     * @param driver   is the class of the database driver
     * @param user     is the name of database user
     * @param password of the user
     * @param size     is maximum number of connections
     * @throws ConnectionPoolException if the error occurred in working with the
     *                                 connection pool
     */
    void init(String url, String driver, String user, String password, int size)
            throws ConnectionPoolException;

    /**
     * <p>This method terminates the connection pool</p>
     *
     * @throws ConnectionPoolException if the error occurred in working with the
     *                                 connection pool
     */
    void destroy() throws ConnectionPoolException;

    /**
     * <p>This method gives the object implements the interface
     * {@link java.sql.Connection}</p>
     *
     * @return
     * @throws ConnectionPoolException if the error occurred in working with the
     *                                 connection pool
     */
    Connection getConnection() throws ConnectionPoolException;
}
