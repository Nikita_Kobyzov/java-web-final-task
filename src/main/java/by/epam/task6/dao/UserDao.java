package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.User;

/**
 * <p>This interface describes the basic methods for DAO that work with User-entity</p>
 * @see by.epam.task6.dao.GenericDao
 * @see by.epam.task6.dao.sql.MySqlUserDao
 * @author Nikita Kobyzov
 */
public interface UserDao extends GenericDao<User, Integer> {
    /**
     * <p>This method reads the user on a login</p>
     *
     * @param login is one of unique identifiers of the user
     * @return the user corresponding to the given login or null if there isn't entity
     * with the appropriate login
     * @throws DaoException if the error occurred in working with the data
     */
    User read(String login) throws DaoException;
}
