package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.to.OrderTO;

import java.util.List;

/**
 * <p>This interface describes the basic methods for DAO that work with Order-entity</p>
 * @see by.epam.task6.dao.GenericDao
 * @see by.epam.task6.dao.sql.MySqlOrderDao
 * @author Nikita Kobyzov
 */
public interface OrderDao extends GenericDao<OrderTO,Integer> {
    /**
     * <p>This method reads the orders on a user unique identifier</p>
     *
     * @param id is a unique identifier of the user
     * @return a list of orders corresponding to the given user id
     * @throws DaoException if the error occurred in working with the data source
     */
    List<OrderTO> readByUserId(Integer id) throws DaoException;
}
