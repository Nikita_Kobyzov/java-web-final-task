package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.to.BookRecordTO;

/**
 * <p>This interface describes the basic methods for DAO that work with BookRecord-entity</p>
 * @see by.epam.task6.dao.GenericDao
 * @see by.epam.task6.dao.sql.MySqlBookRecordDao
 * @author Nikita Kobyzov
 */
public interface BookRecordDao extends GenericDao<BookRecordTO,Integer> {
    /**
     * <p>This method reads the book record on a book unique identifier</p>
     *
     * @param id is a unique identifier of the book
     * @return the object corresponding to the given book identifier or <tt>null</tt>
     * if there isn't entity with the appropriate key
     * @throws DaoException if the error occurred in working with the data source
     */
    BookRecordTO readByBookId(int id) throws DaoException;
}
