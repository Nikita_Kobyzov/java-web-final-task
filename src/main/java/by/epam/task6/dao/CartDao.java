package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.to.CartTO;
import java.util.List;

/**
 * <p>This interface describes the basic methods for DAO that work with Cart-entity</p>
 * @see by.epam.task6.dao.GenericDao
 * @see by.epam.task6.dao.sql.MySqlCartDao
 * @author Nikita Kobyzov
 */
public interface CartDao extends GenericDao<CartTO,Integer> {
    /**
     * <p>This method reads the cart records on a user unique identifier</p>
     *
     * @param id is a unique identifier of the user
     * @return a list of carts corresponding to the given user id
     * @throws DaoException if the error occurred in working with the data source
     */
    List<CartTO> readByUserId(Integer id) throws DaoException;
}
