package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;

import java.util.List;

/**
 * <p>This interface describes the basic methods for classes DAO layer. This
 * interface implements a pattern <strong>DAO</strong></p>
 * @param <T> describes the type of the entity
 * @param <K> describes a unique identifier of the entity
 * @author Nikita Kobyzov
 */
public interface GenericDao<T,K> {
    /**
     * <p>This method writes a new object</p>
     *
     * @param entity is the object that to be recorded
     * @return a created object
     * @throws DaoException if the error occurred in working with the data source
     */
    T create(T entity) throws DaoException;

    /**
     * <p>This method reads the object on a key</p>
     *
     * @param key is a unique identifier of the entity
     * @return the object corresponding to the given key or <tt>null</tt> if
     * there isn't entity with the appropriate key
     * @throws DaoException if the error occurred in working with the data source
     */
    T read(K key) throws DaoException;

    /**
     * <p>This method reads all object</p>
     *
     * @return a list of all objects
     * @throws DaoException if the error occurred in working with the data source
     */
    List<T> read() throws DaoException;

    /**
     * <p>This method update an object in the data source on a key</p>
     *
     * @param key    is a unique identifier of the entity that to be changed
     * @param entity is a object that to be recorded
     * @return an object that has been modified
     * @throws DaoException if the error occurred in working with the data source
     */
    T update(K key, T entity) throws DaoException;

    /**
     * <p>This method delete an object in the data source on a key</p>
     *
     * @param key is a unique identifier of the entity that to be deleted
     * @return an object that has been deleted
     * @throws DaoException if the error occurred in working with the data source
     */
    T delete(K key) throws DaoException;

}