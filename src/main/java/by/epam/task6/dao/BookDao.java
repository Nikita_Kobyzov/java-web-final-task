package by.epam.task6.dao;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.Book;

import java.util.List;

/**
 * <p>This interface describes the basic methods for DAO that work with Book-entity</p>
 * @see by.epam.task6.dao.GenericDao
 * @see by.epam.task6.dao.sql.MySqlBookDao
 * @author Nikita Kobyzov
 */
public interface BookDao extends GenericDao<Book,Integer> {
    /**
     * <p>This method reads the books on a book genre</p>
     *
     * @param genre is a genre of the books
     * @return a list of all books given genre
     * @throws DaoException if the error occurred in working with the data source
     */
    List<Book> readOnGenre(String genre) throws DaoException;
}
