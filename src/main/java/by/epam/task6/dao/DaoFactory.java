package by.epam.task6.dao;

import by.epam.task6.dao.factoryImpl.SqlDaoFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <p>This class is used for classes, working with data source, and hides the
 * details of the implementation of the data source. This class implements
 * a pattern <strong>Factory</strong></p>
 * @author Nikita Kobyzov
 */
public abstract class DaoFactory {
    private static final String DAO_TYPE_KEY = "db.dao_type";
    private static final DaoType DEFAULT_DAO_TYPE = DaoType.SQL;
    private static DaoType daoType;

    static {
        String resourceName = "config.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            props.load(resourceStream);
        } catch (IOException e) {
            daoType = DEFAULT_DAO_TYPE;
        }
        daoType = DaoType.valueOf(props.getProperty(DAO_TYPE_KEY));
    }

    /**
     * <p>This method gives an instance of the object of type
     * {@link by.epam.task6.dao.DaoFactory}</p>
     *
     * @return an an instance of the DAO factory corresponding to the specified type dao.
     * The default DAO type is DAO working with MySQL
     */
    public static DaoFactory getDaoFactory() {
        switch (daoType) {
            case SQL:
                return SqlDaoFactory.getInstance();
            default:
                return SqlDaoFactory.getInstance();
        }
    }

    /**
     * <p>This method gives an instance of the object implements the interface</p>
     * {@link by.epam.task6.dao.UserDao}
     *
     * @return an instance of the object that work with User-entity
     */
    public abstract UserDao getUserDao();

    /**
     * <p>This method gives an instance of the object implements the interface</p>
     * {@link by.epam.task6.dao.BookDao}
     *
     * @return an instance of the object that work with Book-entity
     */
    public abstract BookDao getBookDao();

    /**
     * <p>This method gives an instance of the object implements the interface</p>
     * {@link by.epam.task6.dao.CartDao}
     *
     * @return an instance of the object that work with Cart-entity
     */
    public abstract CartDao getCartDao();

    /**
     * <p>This method gives an instance of the object implements the interface</p>
     * {@link by.epam.task6.dao.OrderDao}
     *
     * @return an instance of the object that work with Order-entity
     */
    public abstract OrderDao getOrderDao();

    /**
     * <p>This method gives an instance of the object implements the interface</p>
     * {@link by.epam.task6.dao.BookRecordDao}
     *
     * @return an instance of the object that work with BookRecord-entity
     */
    public abstract BookRecordDao getBookRecordDao();

    /**
     * <p>This enum contains the types of data sources</p>
     */
    public enum DaoType {
        SQL
    }
}
