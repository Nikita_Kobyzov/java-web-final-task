package by.epam.task6.dao.sql;

import by.epam.task6.dao.BookDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.fieldhelper.BookField;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class works with the entity of the book from the MySQL database</p>
 * @author Nikita Kobyzov
 */
public class MySqlBookDao implements BookDao {
    private static final String FIND_BOOKS_REQUEST = "SELECT * FROM Book";
    private static final String FIND_BOOK_REQUEST = "SELECT * FROM Book WHERE ID=";
    private static final String FIND_BOOK_ON_GENRE_REQUEST = "SELECT * FROM Book WHERE Genre=?";
    private static final String SAVE_BOOK_REQUEST = "INSERT INTO Book" +
            " (Title, Genre, Author, Size, Image,Annotation)" +
            " VALUES (?, ?, ?, ?, ?,?)";
    private static final String GET_LAST_ID = "SELECT MAX(ID) FROM Book";
    private static final String UPDATE_BOOK_REQUEST = "UPDATE Book SET Title=?, Genre=?," +
            " Author=?, Size=?, Image=?, Annotation=? WHERE ID=?";
    private static final String DELETE_BOOK_REQUEST = "DELETE FROM Book WHERE ID=";
    private static final BookDao instance = new MySqlBookDao();

    public static BookDao getInstance() {
        return instance;
    }

    @Override
    public Book read(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_REQUEST + key)) {
            if (resultSet.next()) {
                return this.getBookFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
    }

    @Override
    public List<Book> read() throws DaoException {
        List<Book> bookList;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOKS_REQUEST)) {
            bookList = new ArrayList<Book>();
            Book book;
            while (resultSet.next()) {
                book = this.getBookFromResultSet(resultSet);
                bookList.add(book);
            }
            if (bookList.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return bookList;
    }


    @Override
    public List<Book> readOnGenre(String genre) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Book> list = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_BOOK_ON_GENRE_REQUEST)) {
            statement.setString(1, genre);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    if (list == null) {
                        list = new ArrayList<Book>();
                    }
                    Book book = this.getBookFromResultSet(resultSet);
                    list.add(book);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        if (list == null) {
            return Collections.EMPTY_LIST;
        }
        return list;
    }

    @Override
    public Book create(Book book) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement findIdStatement = connection.createStatement();
                 PreparedStatement statement = connection.prepareStatement(SAVE_BOOK_REQUEST)) {
                statement.setString(1, book.getTitle());
                statement.setString(2, book.getGenre());
                statement.setString(3, book.getAuthor());
                statement.setInt(4, book.getSize());
                statement.setString(5, book.getImage());
                statement.setString(6, book.getAnnotation());
                statement.executeUpdate();
                try (ResultSet resultSet = findIdStatement.executeQuery(GET_LAST_ID)) {
                    resultSet.next();
                    book.setId(resultSet.getInt(1));
                    connection.commit();
                } catch (SQLException e) {
                    throw new DaoException("Exception working with the database", e);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return book;
    }

    @Override
    public Book update(Integer key, Book book) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Book updatedBook;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement updateStatement = connection.prepareStatement(UPDATE_BOOK_REQUEST);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_REQUEST + key)) {
            resultSet.next();
            updatedBook = this.getBookFromResultSet(resultSet);
            updateStatement.setString(1, book.getTitle());
            updateStatement.setString(2, book.getGenre());
            updateStatement.setString(3, book.getAuthor());
            updateStatement.setInt(4, book.getSize());
            updateStatement.setString(5, book.getImage());
            updateStatement.setString(6, book.getAnnotation());
            updateStatement.setInt(7, key);
            updateStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return updatedBook;
    }

    @Override
    public Book delete(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Book deletedBook;
        try (Connection connection = connectionPool.getConnection();
             Statement deleteStatement = connection.createStatement();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_REQUEST + key)) {
            resultSet.next();
            deletedBook = this.getBookFromResultSet(resultSet);
            deleteStatement.executeUpdate(DELETE_BOOK_REQUEST + key);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return deletedBook;
    }

    /**
     * <p>This method reads one record from the {@link java.sql.ResultSet}</p>
     *
     * @param resultSet is an resulting query
     * @return an object of class {@link by.epam.task6.domain.Book}
     * @throws SQLException if the error occurred in working with the database
     */
    private Book getBookFromResultSet(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        book.setId(resultSet.getInt(BookField.ID));
        book.setTitle(resultSet.getString(BookField.TITLE));
        book.setGenre(resultSet.getString(BookField.GENRE));
        book.setAuthor(resultSet.getString(BookField.AUTHOR));
        book.setImage(resultSet.getString(BookField.IMAGE));
        book.setSize(resultSet.getInt(BookField.SIZE));
        book.setAnnotation(resultSet.getString(BookField.ANNOTATION));
        return book;
    }
}