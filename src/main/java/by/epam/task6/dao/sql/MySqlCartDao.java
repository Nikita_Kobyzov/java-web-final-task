package by.epam.task6.dao.sql;

import by.epam.task6.dao.CartDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.domain.fieldhelper.CartField;
import by.epam.task6.domain.to.CartTO;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class works with the entity of the cart from the MySQL database</p>
 * @author Nikita Kobyzov
 */
public class MySqlCartDao implements CartDao {
    private static final String FIND_CART_REQUEST = "SELECT * FROM Cart WHERE ID=";
    private static final String FIND_CARTS_REQUEST = "SELECT * FROM Cart";
    private static final String FIND_CART_ON_USER_REQUEST = "SELECT * FROM Cart" +
            " WHERE id_client=";
    private static final String SAVE_CART_REQUEST = "INSERT INTO Cart" +
            " (id_client,id_book) VALUES (?,?)";
    private static final String UPDATE_CART_REQUEST = "UPDATE Cart SET" +
            " id_book=?, id_client=? WHERE ID=?";
    private static final String DELETE_CART_REQUEST = "DELETE FROM Cart WHERE ID=";
    private static final String GET_LAST_ID = "SELECT last_insert_id() AS last_id FROM Cart";
    private static final MySqlCartDao instance = new MySqlCartDao();

    private MySqlCartDao() {
    }

    public static MySqlCartDao getInstance() {
        return instance;
    }

    @Override
    public CartTO read(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_CART_REQUEST + key)) {
            if (resultSet.next()) {
                return this.getCartFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
    }

    @Override
    public List<CartTO> read() throws DaoException {
        List<CartTO> list = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_CARTS_REQUEST)) {
            CartTO cart;
            while (resultSet.next()) {
                if (list == null) {
                    list = new ArrayList<CartTO>();
                }
                cart = this.getCartFromResultSet(resultSet);
                list.add(cart);
            }
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return list;
    }

    @Override
    public List<CartTO> readByUserId(Integer id) throws DaoException {
        List<CartTO> list = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_CART_ON_USER_REQUEST + id)) {
            CartTO cart;
            while (resultSet.next()) {
                if (list == null) {
                    list = new ArrayList<CartTO>();
                }
                cart = this.getCartFromResultSet(resultSet);
                list.add(cart);
            }
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return list;
    }

    @Override
    public CartTO create(CartTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement findIdStatement = connection.createStatement();
                 PreparedStatement statement = connection.prepareStatement(SAVE_CART_REQUEST)) {
                statement.setInt(1, entity.getId_client());
                statement.setInt(2, entity.getId_book());
                statement.executeUpdate();
                try (ResultSet resultSet = findIdStatement.executeQuery(GET_LAST_ID)) {
                    resultSet.next();
                    entity.setId(resultSet.getInt(1));
                    connection.commit();
                } catch (SQLException e) {
                    throw new DaoException("Exception in working with the database", e);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return entity;
    }

    @Override
    public CartTO update(Integer key, CartTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        CartTO updatedCart;
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_CART_REQUEST + key);
             PreparedStatement updateStatement = connection.prepareStatement(UPDATE_CART_REQUEST)) {
            resultSet.next();
            updatedCart = this.getCartFromResultSet(resultSet);
            updateStatement.setInt(1, entity.getId_client());
            updateStatement.setInt(2, entity.getId_book());
            updateStatement.setInt(3, entity.getId());
            updateStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return updatedCart;
    }

    @Override
    public CartTO delete(Integer key) throws DaoException {
        CartTO deletedCart;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement deleteStatement = connection.createStatement();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_CART_REQUEST + key)) {
            resultSet.next();
            deletedCart = this.getCartFromResultSet(resultSet);
            deleteStatement.executeUpdate(DELETE_CART_REQUEST + key);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return deletedCart;
    }

    /**
     * <p>This method reads one record from the {@link java.sql.ResultSet}</p>
     *
     * @param resultSet is an resulting query
     * @return an object of class {@link by.epam.task6.domain.to.CartTO}
     * @throws SQLException if the error occurred in working with the database
     */
    private CartTO getCartFromResultSet(ResultSet resultSet) throws SQLException {
        CartTO cart = new CartTO();
        cart.setId(resultSet.getInt(CartField.ID));
        cart.setId_book(resultSet.getInt(CartField.ID_BOOK));
        cart.setId_client(resultSet.getInt(CartField.ID_CLIENT));
        return cart;
    }
}
