package by.epam.task6.dao.sql;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.OrderDao;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.domain.fieldhelper.OrderField;
import by.epam.task6.domain.to.OrderTO;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class works with the entity of the order from the MySQL database</p>
 * @author Nikita Kobyzov
 */
public class MySqlOrderDao implements OrderDao {
    private static final String FIND_ORDER_REQUEST = "SELECT * FROM `Order` WHERE ID=";
    private static final String FIND_ORDERS_REQUEST = "SELECT * FROM `Order`";
    private static final String FIND_ORDER_ON_USER_REQUEST = "SELECT * FROM `Order`" +
            " WHERE ID_Client=";
    private static final String SAVE_ORDER_REQUEST = "INSERT INTO `Order`" +
            " (id_client,id_book,confirmation,reading_place) VALUES (?,?,?,?)";
    private static final String GET_LAST_ID = "SELECT MAX(ID) FROM `Order`";
    private static final String UPDATE_ORDER_REQUEST = "UPDATE `Order` SET" +
            " confirmation=?,reading_place=? WHERE ID=?";
    private static final String DELETE_ORDER_REQUEST = "DELETE FROM `Order` WHERE ID=";
    private static final MySqlOrderDao instance = new MySqlOrderDao();

    private MySqlOrderDao() {
    }

    public static MySqlOrderDao getInstance() {
        return instance;
    }

    @Override
    public OrderTO read(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ORDER_REQUEST + key)) {
            if (resultSet.next()) {
                return this.getOrderFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
    }

    @Override
    public List<OrderTO> read() throws DaoException {
        List<OrderTO> list = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ORDERS_REQUEST)) {
            OrderTO order;
            while (resultSet.next()) {
                if (list == null) {
                    list = new ArrayList<OrderTO>();
                }
                order = this.getOrderFromResultSet(resultSet);
                list.add(order);
            }
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return list;
    }

    @Override
    public List<OrderTO> readByUserId(Integer id) throws DaoException {
        List<OrderTO> list = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ORDER_ON_USER_REQUEST
                     + id)) {
            OrderTO order;
            while (resultSet.next()) {
                if (list == null) {
                    list = new ArrayList<OrderTO>();
                }
                order = this.getOrderFromResultSet(resultSet);
                list.add(order);
            }
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return list;
    }

    @Override
    public OrderTO create(OrderTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement findIdStatement = connection.createStatement();
                 PreparedStatement statement = connection.prepareStatement(SAVE_ORDER_REQUEST)) {
                statement.setInt(1, entity.getId_client());
                statement.setInt(2, entity.getId_book());
                statement.setBoolean(3, entity.isConfirmation());
                statement.setString(4, entity.getReadingPlace());
                statement.executeUpdate();
                try (ResultSet resultSet = findIdStatement.executeQuery(GET_LAST_ID)) {
                    resultSet.next();
                    entity.setId(resultSet.getInt(1));
                    connection.commit();
                } catch (SQLException e) {
                    throw new DaoException("Exception in working with the database", e);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in MySQL DAO", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return entity;
    }

    @Override
    public OrderTO update(Integer key, OrderTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        OrderTO updatedOrder;
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ORDER_REQUEST + key);
             PreparedStatement UpdateStatement = connection.prepareStatement(UPDATE_ORDER_REQUEST)) {
            resultSet.next();
            updatedOrder = this.getOrderFromResultSet(resultSet);
            UpdateStatement.setBoolean(1, entity.isConfirmation());
            UpdateStatement.setString(2, entity.getReadingPlace());
            UpdateStatement.setInt(3, key);
            UpdateStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return updatedOrder;
    }

    @Override
    public OrderTO delete(Integer key) throws DaoException {
        OrderTO deletedOrder;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement deleteStatement = connection.createStatement();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ORDER_REQUEST + key)) {
            resultSet.next();
            deletedOrder = this.getOrderFromResultSet(resultSet);
            deleteStatement.executeUpdate(DELETE_ORDER_REQUEST + key);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return deletedOrder;
    }

    /**
     * <p>This method reads one record from the {@link java.sql.ResultSet}</p>
     *
     * @param resultSet is an resulting query
     * @return an object of class {@link by.epam.task6.domain.to.OrderTO}
     * @throws SQLException if the error occurred in working with the database
     */
    private OrderTO getOrderFromResultSet(ResultSet resultSet) throws SQLException {
        OrderTO order = new OrderTO();
        order.setId(resultSet.getInt(OrderField.ID));
        order.setConfirmation(resultSet.getBoolean(OrderField.CONFIRMATION));
        order.setReadingPlace(resultSet.getString(OrderField.READING_PLACE));
        order.setId_book(resultSet.getInt(OrderField.ID_BOOK));
        order.setId_client(resultSet.getInt(OrderField.ID_CLIENT));
        return order;
    }
}
