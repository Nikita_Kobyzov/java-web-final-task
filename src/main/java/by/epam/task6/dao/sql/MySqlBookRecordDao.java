package by.epam.task6.dao.sql;

import by.epam.task6.dao.BookRecordDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.domain.fieldhelper.BookRecordField;
import by.epam.task6.domain.to.BookRecordTO;


import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class works with the entity of the book record from the MySQL database</p>
 * @author Nikita Kobyzov
 */
public class MySqlBookRecordDao implements BookRecordDao {
    private static final String FIND_BOOK_RECORDS_REQUEST = "SELECT * FROM Book_RECORD";
    private static final String FIND_BOOK_RECORD_REQUEST = "SELECT * FROM Book_RECORD WHERE ID=";
    private static final String FIND_BOOK_RECORD_ON_BOOK_REQUEST = "SELECT * FROM Book_RECORD" +
            " WHERE ID_Book=";
    private static final String SAVE_BOOK_RECORD_REQUEST = "INSERT INTO Book_RECORD" +
            " (ID_Book, Total_count, free_count)  VALUES (?, ?,?)";
    private static final String GET_LAST_ID = "SELECT MAX(ID) FROM Book_Record";
    private static final String UPDATE_BOOK_RECORD_REQUEST = "UPDATE Book_RECORD SET Total_count=?," +
            " free_count=? WHERE ID=?";
    private static final String DELETE_BOOK_RECORD_REQUEST = "DELETE FROM Book_RECORD WHERE ID=?";
    private static final MySqlBookRecordDao instance = new MySqlBookRecordDao();

    private MySqlBookRecordDao() {
    }

    public static MySqlBookRecordDao getInstance() {
        return instance;
    }


    @Override
    public BookRecordTO read(Integer key) throws DaoException {
        BookRecordTO bookRecordTO;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_RECORD_REQUEST + key)) {
            if (resultSet.next()) {
                bookRecordTO = this.getBookRecordFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return bookRecordTO;
    }

    @Override
    public List<BookRecordTO> read() throws DaoException {
        List<BookRecordTO> bookRecordTOList = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_RECORDS_REQUEST)) {
            BookRecordTO bookRecordTO;
            while (resultSet.next()) {
                if (bookRecordTOList == null) {
                    bookRecordTOList = new ArrayList<BookRecordTO>();
                }
                bookRecordTO = this.getBookRecordFromResultSet(resultSet);
                bookRecordTOList.add(bookRecordTO);
            }
            if (bookRecordTOList == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return bookRecordTOList;
    }

    @Override
    public BookRecordTO readByBookId(int id) throws DaoException {
        BookRecordTO bookRecordTO;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_RECORD_ON_BOOK_REQUEST
                     + id)) {
            if (resultSet.next()) {
                bookRecordTO = this.getBookRecordFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return bookRecordTO;
    }

    @Override
    public BookRecordTO create(BookRecordTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement findIdStatement = connection.createStatement();
                 PreparedStatement statement = connection.prepareStatement(SAVE_BOOK_RECORD_REQUEST)) {
                statement.setInt(1, entity.getId_book());
                statement.setInt(2, entity.getTotalCount());
                statement.setInt(3, entity.getFreeCount());
                statement.executeUpdate();
                try (ResultSet resultSet = findIdStatement.executeQuery(GET_LAST_ID)) {
                    resultSet.next();
                    entity.setId(resultSet.getInt(1));
                    connection.commit();
                } catch (SQLException e) {
                    throw new DaoException("Exception in working with the database", e);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return entity;
    }

    @Override
    public BookRecordTO update(Integer key, BookRecordTO entity) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        BookRecordTO bookRecordTO;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement updateStatement = connection.prepareStatement(UPDATE_BOOK_RECORD_REQUEST);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_RECORD_REQUEST + key)) {
            resultSet.next();
            bookRecordTO = this.getBookRecordFromResultSet(resultSet);
            updateStatement.setInt(1, entity.getTotalCount());
            updateStatement.setInt(2, entity.getFreeCount());
            updateStatement.setInt(3, key);
            updateStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return bookRecordTO;
    }

    @Override
    public BookRecordTO delete(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        BookRecordTO bookRecordTO;
        try (Connection connection = connectionPool.getConnection();
             Statement deleteStatement = connection.createStatement();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_BOOK_RECORD_REQUEST + key)) {
            resultSet.next();
            bookRecordTO = this.getBookRecordFromResultSet(resultSet);
            deleteStatement.executeUpdate(DELETE_BOOK_RECORD_REQUEST + key);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Error in working with the database", e);
        }
        return bookRecordTO;
    }

    /**
     * <p>This method reads one record from the {@link java.sql.ResultSet}</p>
     *
     * @param resultSet is an resulting query
     * @return an object of class {@link by.epam.task6.domain.to.BookRecordTO}
     * @throws SQLException if the error occurred in working with the database
     */
    private BookRecordTO getBookRecordFromResultSet(ResultSet resultSet) throws SQLException {
        BookRecordTO bookRecordTO = new BookRecordTO();
        bookRecordTO.setId(resultSet.getInt(BookRecordField.ID));
        bookRecordTO.setId_book(resultSet.getInt(BookRecordField.ID_BOOK));
        bookRecordTO.setTotalCount(resultSet.getInt(BookRecordField.TOTAL_COUNT));
        bookRecordTO.setFreeCount(resultSet.getInt(BookRecordField.FREE_COUNT));
        return bookRecordTO;
    }
}