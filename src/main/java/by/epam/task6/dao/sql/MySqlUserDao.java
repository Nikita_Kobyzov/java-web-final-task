package by.epam.task6.dao.sql;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.UserDao;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.UserField;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class works with the entity of the user from the MySQL database</p>
 * @author Nikita Kobyzov
 */
public class MySqlUserDao implements UserDao {
    private static final String FIND_USERS_REQUEST = "SELECT * FROM USER";
    private static final String FIND_USER_REQUEST = "SELECT * FROM USER WHERE ID=";
    private static final String SAVE_USER_REQUEST = "INSERT INTO user" +
            " ( Login, Password, Number, FName, LName, User_status)" +
            " VALUES (?, ?, ?, ?, ?, ?)";
    private static final String GET_LAST_ID = "SELECT MAX(ID) FROM User";
    private static final String FIND_USER_ON_LOGIN_REQUEST = "SELECT * FROM USER WHERE Login=?";
    private static final String UPDATE_USER_REQUEST = "UPDATE USER SET Login=?," +
            " Password=?, Number=?, FName=?, LName=?, User_status=? WHERE ID=?";
    private static final String DELETE_USER_REQUEST = "DELETE FROM User WHERE ID=";
    private static final UserDao instance = new MySqlUserDao();

    public static UserDao getInstance() {
        return instance;
    }

    @Override
    public User read(Integer key) throws DaoException {
        User user;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_USER_REQUEST + key)) {
            if (resultSet.next()) {
                user = this.getUserFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return user;
    }

    @Override
    public List<User> read() throws DaoException {
        List<User> list = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_USERS_REQUEST)) {
            User user;
            while (resultSet.next()) {
                if (list == null) {
                    list = new ArrayList<User>();
                }
                user = this.getUserFromResultSet(resultSet);
                list.add(user);
            }
            if (list == null) {
                return Collections.EMPTY_LIST;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return list;
    }

    @Override
    public User read(String login) throws DaoException {
        User user;
        ResultSet resultSet = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_USER_ON_LOGIN_REQUEST)) {
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = this.getUserFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        } finally {

        }
        return user;
    }

    @Override
    public User create(User user) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement findIdStatement = connection.createStatement();
                 PreparedStatement statement = connection.prepareStatement(SAVE_USER_REQUEST)) {
                statement.setString(1, user.getLogin());
                statement.setString(2, user.getPassword());
                statement.setInt(3, user.getNumber());
                statement.setString(4, user.getfName());
                statement.setString(5, user.getlName());
                statement.setString(6, user.getUserStatus().toString());
                statement.executeUpdate();
                try (ResultSet resultSet = findIdStatement.executeQuery(GET_LAST_ID)) {
                    resultSet.next();
                    user.setId(resultSet.getInt(1));
                    connection.commit();
                } catch (SQLException e) {
                    throw new DaoException("Exception in MySQL working with the database", e);
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in MySQL working with the database", e);
            }
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in MySQL working with the database", e);
        }
        return user;
    }

    @Override
    public User update(Integer key, User user) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        User updatedUser;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement updateStatement = connection.prepareStatement(UPDATE_USER_REQUEST);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_USER_REQUEST + key)) {
            resultSet.next();
            updatedUser = this.getUserFromResultSet(resultSet);
            updateStatement.setString(1, user.getLogin());
            updateStatement.setString(2, user.getPassword());
            updateStatement.setInt(3, user.getNumber());
            updateStatement.setString(4, user.getfName());
            updateStatement.setString(5, user.getlName());
            updateStatement.setString(6, user.getUserStatus().toString());
            updateStatement.setInt(7, user.getId());
            updateStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        }
        return updatedUser;
    }

    @Override
    public User delete(Integer key) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        User deletedUser;
        try (Connection connection = connectionPool.getConnection();
             Statement deleteStatement = connection.createStatement();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_USER_REQUEST + key)) {
            resultSet.next();
            deletedUser = this.getUserFromResultSet(resultSet);
            deleteStatement.executeUpdate(DELETE_USER_REQUEST + key);
        } catch (ConnectionPoolException | SQLException e) {
            throw new DaoException("Error in working with the database", e);
        }
        return deletedUser;
    }

    /**
     * <p>This method reads one record from the {@link java.sql.ResultSet}</p>
     *
     * @param resultSet is an resulting query
     * @return an object of class {@link by.epam.task6.domain.User}
     * @throws SQLException if the error occurred in working with the database
     */
    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setLogin(resultSet.getString(UserField.LOGIN));
        user.setPassword(resultSet.getString(UserField.PASSWORD));
        user.setId(resultSet.getInt(UserField.ID));
        user.setNumber(resultSet.getInt(UserField.NUMBER));
        user.setfName(resultSet.getString(UserField.FNAME));
        user.setlName(resultSet.getString(UserField.LNAME));
        user.setUserStatus(UserStatus.valueOf(
                resultSet.getString(UserField.USER_STATUS).toUpperCase()));
        return user;
    }
}
