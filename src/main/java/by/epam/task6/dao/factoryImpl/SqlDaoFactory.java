package by.epam.task6.dao.factoryImpl;

import by.epam.task6.dao.*;
import by.epam.task6.dao.sql.*;

/**
 * <p>This class is used to receive objects which works with MySQL database</p>
 * @see by.epam.task6.dao.DaoFactory
 * @author Nikita Kobyzov
 */
public class SqlDaoFactory extends DaoFactory {
    private static final SqlDaoFactory INSTANCE = new SqlDaoFactory();

    public static SqlDaoFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public UserDao getUserDao() {
        return MySqlUserDao.getInstance();
    }

    @Override
    public BookDao getBookDao() {
        return MySqlBookDao.getInstance();
    }

    @Override
    public CartDao getCartDao() {
        return MySqlCartDao.getInstance();
    }

    @Override
    public OrderDao getOrderDao() {
        return MySqlOrderDao.getInstance();
    }

    @Override
    public BookRecordDao getBookRecordDao() {
        return MySqlBookRecordDao.getInstance();
    }
}
