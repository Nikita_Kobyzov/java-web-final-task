package by.epam.task6.validator;

import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.to.OrderTO;
import by.epam.task6.validator.impl.BookRecordValidator;
import by.epam.task6.validator.impl.BookValidator;
import by.epam.task6.validator.impl.OrderValidator;
import by.epam.task6.validator.impl.UserValidator;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class is used to contain the instances of classes that implements
 * the interface {@link by.epam.task6.validator.IValidator}. This interface
 * implements a pattern <strong>Factory</strong></p>
 * @author Nikita Kobyzov
 */
public final class ValidatorFactory {
    private static final Map<Class, IValidator> map = new HashMap<Class, IValidator>();

    public ValidatorFactory() {
        map.put(User.class, new UserValidator());
        map.put(Book.class, new BookValidator());
        map.put(BookRecordTO.class, new BookRecordValidator());
        map.put(OrderTO.class, new OrderValidator());
    }

    /**
     * <p>This method is used to take a validator, corresponding to the given object</p>
     *
     * @param className is class of the object, which should be validated
     * @return the validator corresponding to the given class type
     */
    public IValidator getValidator(Class className) {
        return map.get(className);
    }
}
