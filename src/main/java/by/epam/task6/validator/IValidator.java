package by.epam.task6.validator;

import by.epam.task6.validator.exception.ValidatorException;

/**
 * <p>This interface describes the basic method of the validators</p>
 * @param <T> describes the type of the entity, which should be validated
 * @see by.epam.task6.validator.Validator
 * @see by.epam.task6.validator.impl.UserValidator
 * @see by.epam.task6.validator.impl.BookValidator
 * @see by.epam.task6.validator.impl.BookRecordValidator
 * @see by.epam.task6.validator.impl.OrderValidator
 * @author Nikita Kobyzov
 */
public interface IValidator<T> {
    /**
     * <p>This method validate the entity</p>
     *
     * @param entity is the object, which should be validated
     * @return <tt>true</tt> if the object is valid
     * @throws ValidatorException if the error occurred in validating or the
     *                            object is not valid
     */
    boolean validate(T entity) throws ValidatorException;
}
