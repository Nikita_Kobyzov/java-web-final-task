package by.epam.task6.validator.exception;

/**
 * <p>This exception throws if the error occurred in validating or the
 * object is not valid. If this exception throws after because of invalid object,
 * the message would have a description of the invalid field</p>
 * @author Nikita Kobyzov
 */
public class ValidatorException extends Exception {
    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
