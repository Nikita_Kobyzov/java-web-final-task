package by.epam.task6.validator.impl;

import by.epam.task6.domain.Book;
import by.epam.task6.validator.IValidator;
import by.epam.task6.validator.exception.ValidatorException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

/**
 * <p>This class is used to validate an object of class
 * {@link by.epam.task6.domain.Book}</p>
 * @see by.epam.task6.validator.IValidator
 * @author Nikita Kobyzov
 */
public class BookValidator implements IValidator<Book> {
    private static final Logger LOG = LogManager.getLogger(BookValidator.class);
    private static final String TITLE_REGULAR = "[а-яА-яa-zA-z0-9\\.', ]+";
    private static final String GENRE_REGULAR = "[a-zA-z_]+";
    private static final String IMAGE_REGULAR = "[0-9a-zA-Zа-яА-Я]+\\.(jpg|png|jpeg)";
    private static final String ANNOTATION_REGULAR = ".*";
    private static final Pattern TITLE_PATTERN = Pattern.compile(TITLE_REGULAR);
    private static final Pattern GENRE_PATTERN = Pattern.compile(GENRE_REGULAR);
    private static final Pattern IMAGE_PATTERN = Pattern.compile(IMAGE_REGULAR);
    private static final Pattern ANNOTATION_PATTERN = Pattern.compile(ANNOTATION_REGULAR);

    @Override
    public boolean validate(Book entity) throws ValidatorException {
        if (entity.getTitle() != null) {
            if (!TITLE_PATTERN.matcher(entity.getTitle()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book title is not valid");
            }
        }
        if (entity.getAuthor() != null) {
            if (!TITLE_PATTERN.matcher(entity.getAuthor()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book author is not valid");
            }
        }
        if (entity.getGenre() != null) {
            if (!GENRE_PATTERN.matcher(entity.getGenre()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book genre is not valid");
            }
        }
        if (entity.getImage() != null) {
            if (!IMAGE_PATTERN.matcher(entity.getImage()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book image is not valid");
            }
        }
        if (entity.getSize() != null) {
            if (entity.getSize() < 0) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book size is not valid");
            }
        }
        if (entity.getAnnotation() != null) {
            if (!ANNOTATION_PATTERN.matcher(entity.getAnnotation()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book annotation is not valid");
            }
        }
        LOG.info("Object " + entity + " is valid");
        return true;
    }
}
