package by.epam.task6.validator.impl;

import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.validator.IValidator;
import by.epam.task6.validator.exception.ValidatorException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * <p>This class is used to validate an object of class
 * {@link by.epam.task6.domain.to.BookRecordTO}</p>
 * @see by.epam.task6.validator.IValidator
 * @author Nikita Kobyzov
 */
public class BookRecordValidator implements IValidator<BookRecordTO> {
    private static final Logger LOG = LogManager.getLogger(BookRecordValidator.class);

    @Override
    public boolean validate(BookRecordTO entity) throws ValidatorException {
        if (entity.getFreeCount() != null) {
            if (entity.getFreeCount() < 0) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book Record free count is not valid");
            }
        }
        if (entity.getTotalCount() != null) {
            if (entity.getTotalCount() < 0) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Book Record total count is not valid");
            }
        }
        LOG.info("Object " + entity + " is valid");
        return true;
    }
}
