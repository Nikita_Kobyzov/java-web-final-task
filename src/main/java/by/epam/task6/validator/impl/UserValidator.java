package by.epam.task6.validator.impl;

import by.epam.task6.domain.User;
import by.epam.task6.validator.IValidator;
import by.epam.task6.validator.exception.ValidatorException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

/**
 * <p>This class is used to validate an object of class
 * {@link by.epam.task6.domain.User}</p>
 * @see by.epam.task6.validator.IValidator
 * @author Nikita Kobyzov
 */
public class UserValidator implements IValidator<User> {
    private static final Logger LOG = LogManager.getLogger(UserValidator.class);
    private static final String LOGIN_REGULAR = "[a-zA-Z0-9_@/.]{5,45}";
    private static final String NUMBER_REGULAR = "[0-9]{9}";
    private static final String NAME_REGULAR = "[а-яА-яa-zA-z ]+";
    private static final Pattern LOGIN_PATTERN = Pattern.compile(LOGIN_REGULAR);
    private static final Pattern NUMBER_PATTERN = Pattern.compile(NUMBER_REGULAR);
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGULAR);

    @Override
    public boolean validate(User entity) throws ValidatorException {
        if (entity.getLogin() != null) {
            if (!LOGIN_PATTERN.matcher(entity.getLogin()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("User login is not valid");
            }
        }
        if (entity.getPassword() != null) {
            if (!LOGIN_PATTERN.matcher(entity.getPassword()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("User password is not valid");
            }
        }

        if (entity.getNumber() != null) {
            if (!NUMBER_PATTERN.matcher(entity.getNumber().toString()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("User number is not valid");
            }
        }
        if (entity.getfName() != null) {
            if (!NAME_PATTERN.matcher(entity.getfName()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("User first name is not valid");
            }
        }
        if (entity.getlName() != null) {
            if (!NAME_PATTERN.matcher(entity.getlName()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("User last name is not valid");
            }
        }
        LOG.info("Object " + entity + " is valid");
        return true;
    }
}
