package by.epam.task6.validator.impl;

import by.epam.task6.domain.to.OrderTO;
import by.epam.task6.validator.IValidator;
import by.epam.task6.validator.exception.ValidatorException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

/**
 * <p>This class is used to validate an object of class
 * {@link by.epam.task6.domain.to.OrderTO}</p>
 * @see by.epam.task6.validator.IValidator
 * @author Nikita Kobyzov
 */
public class OrderValidator implements IValidator<OrderTO> {
    private static final Logger LOG = LogManager.getLogger(OrderValidator.class);
    private static final String READING_PLACE_REGULAR = "(at_hall)|(on_subscription)";
    private static final Pattern READING_PLACE_PATTERN = Pattern.compile(READING_PLACE_REGULAR);

    @Override
    public boolean validate(OrderTO entity) throws ValidatorException {
        if (entity.getReadingPlace() != null) {
            if (!READING_PLACE_PATTERN.matcher(entity.getReadingPlace()).matches()) {
                LOG.warn("Object " + entity + " is not valid");
                throw new ValidatorException("Order reading place is not valid");
            }
        }
        LOG.info("Object " + entity + " is valid");
        return true;
    }
}
