package by.epam.task6.validator;

import by.epam.task6.validator.exception.ValidatorException;

/**
 * <p>This class is used to validate entities</p>
 * @see by.epam.task6.validator.IValidator
 * @see by.epam.task6.validator.impl.UserValidator
 * @see by.epam.task6.validator.impl.BookValidator
 * @see by.epam.task6.validator.impl.BookRecordValidator
 * @see by.epam.task6.validator.impl.OrderValidator
 * @author Nikita Kobyzov
 */
public class Validator implements IValidator {
    private static final ValidatorFactory FACTORY = new ValidatorFactory();
    private static final Validator INSTANCE = new Validator();

    private Validator() {
    }

    public static Validator getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean validate(Object entity) throws ValidatorException {
        IValidator validator = FACTORY.getValidator(entity.getClass());
        return validator.validate(entity);
    }
}
