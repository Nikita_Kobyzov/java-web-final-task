package by.epam.task6.controller;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * <p>This class is used to receive and process http requests sent from the client
 * and to generate a http response. It is used as a main controller for a web
 * application "Digital library"</p>
 * @author Nikita Kobyzov
 */
@MultipartConfig
public class LibraryController extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(LibraryController.class);

    /**
     * <p>This field is used as a factory to receive commands</p>
     *
     * @see by.epam.task6.controller.CommandFactory
     * @see by.epam.task6.controller.command.ICommand
     */
    private static final CommandFactory COMMAND_FACTORY = CommandFactory.getInstance();

    /**
     * <p>This field is used as a default page when the error occurred while
     * processing the request</p>
     */
    private static final String DEFAULT_PAGE = "index.jsp";

    /**
     * <p>This field is used as a key to receive a command name from request</p>
     */
    private static final String ACTION_KEY = "ACTION";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String urlPath = doProcess(request, response);
        response.sendRedirect(urlPath);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String urlPath = doProcess(request, response);
        request.getRequestDispatcher(urlPath).forward(request, response);
    }

    /**
     * <p>This method is used to process requests and to generate a response</p>
     *
     * @param request is http request sent from the client
     * @param response is http response for the client
     * @return the url
     * @throws ServletException if an error has occurred on the server
     * @throws IOException      if an error has occurred with the failed or interrupted
     *                          I/O operations
     */
    private String doProcess(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String urlPath;
        try {
            String commandName = request.getParameter(ACTION_KEY);
            ICommand command = COMMAND_FACTORY.getCommand(commandName);
            urlPath = command.execute(request);
        } catch (CommandException e) {
            LOG.error("Exception in Library Controller", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return DEFAULT_PAGE;
        } catch (Exception e) {
            LOG.error("Exception in Library Controller", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return DEFAULT_PAGE;
        }
        return urlPath;
    }
}
