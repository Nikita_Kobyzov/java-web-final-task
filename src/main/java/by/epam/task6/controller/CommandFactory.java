package by.epam.task6.controller;

import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.impl.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class is used to contain the instances of classes that implements
 * the interface {@link by.epam.task6.controller.command.ICommand}. This class
 * implements a pattern <strong>Factory</strong></p>
 * @author Nikita Kobyzov
 */
public final class CommandFactory {
    private static final CommandFactory INSTANCE = new CommandFactory();
    private Map<CommandName, ICommand> commandMap = new HashMap<CommandName, ICommand>();

    private CommandFactory() {
        commandMap.put(CommandName.LOGIN, new Login());
        commandMap.put(CommandName.LOGOUT, new Logout());
        commandMap.put(CommandName.CHANGE_LANGUAGE, new ChangeLanguage());
        commandMap.put(CommandName.REGISTRATION, new Registration());
        commandMap.put(CommandName.MAIN_PAGE, new MainPage());
        commandMap.put(CommandName.BOOK_VIEW, new BookView());
        commandMap.put(CommandName.DELETE_BOOK, new DeleteBook());
        commandMap.put(CommandName.ADD_BOOK, new AddBook());
        commandMap.put(CommandName.SELECT_ON_GENRE, new SelectOnGenre());
        commandMap.put(CommandName.BOOK_EDITOR, new BookEditor());
        commandMap.put(CommandName.TO_CART, new ToCart());
        commandMap.put(CommandName.USER_CART, new UserCart());
        commandMap.put(CommandName.TAKE_BOOK, new TakeBook());
        commandMap.put(CommandName.REMOVE_BOOK_FROM_CART, new RemoveBookFromCart());
        commandMap.put(CommandName.EDIT_BOOK, new EditBook());
        commandMap.put(CommandName.ORDER_LIST, new OrderList());
        commandMap.put(CommandName.CONFIRM_ORDER, new ConfirmOrder());
        commandMap.put(CommandName.DELETE_ORDER, new DeleteOrder());
        commandMap.put(CommandName.ADMIN_ORDER_LIST, new AdminOrderList());
    }

    public static CommandFactory getInstance() {
        return INSTANCE;
    }

    /**
     * <p>This method is used to take a command, corresponding to the given
     * command name</p>
     *
     * @param commandName is a name of a received command
     * @return a command corresponding to the given command name
     */
    public ICommand getCommand(String commandName) {
        CommandName command = CommandName.valueOf(commandName.toUpperCase());
        return commandMap.get(command);
    }

    /**
     * <p>This enum consists the names of all used commands</p>
     */
    public enum CommandName {
        LOGIN, LOGOUT, CHANGE_LANGUAGE, REGISTRATION, MAIN_PAGE, BOOK_VIEW, DELETE_BOOK,
        ADD_BOOK, SELECT_ON_GENRE, BOOK_EDITOR, ADMIN_ORDER_LIST, USER_CART, TAKE_BOOK,
        REMOVE_BOOK_FROM_CART, EDIT_BOOK, ORDER_LIST, CONFIRM_ORDER, DELETE_ORDER, TO_CART
    }
}
