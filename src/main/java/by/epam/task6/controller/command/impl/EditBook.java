package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.BookField;
import by.epam.task6.domain.fieldhelper.BookRecordField;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.vo.BookRecordVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;
import by.epam.task6.service.impl.BookService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.*;

/**
 * <p>This class is used to edit book</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class EditBook implements ICommand {
    private static final String MAIN_PAGE_COMMAND_KEY = "libraryController?ACTION=MAIN_PAGE";
    private static final String DIRECTION = "image\\book\\";
    private static final String EXPANSION = ".jpeg";
    private static final String LAST_IMAGE_KEY = "LAST_IMAGE";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String title = request.getParameter(BookField.TITLE).trim();
        String author = request.getParameter(BookField.AUTHOR).trim();
        String genre = request.getParameter(BookField.GENRE).trim();
        String size = request.getParameter(BookField.SIZE).trim();
        String count = request.getParameter(BookRecordField.FREE_COUNT).trim();
        String annotation = request.getParameter(BookField.ANNOTATION).trim();
        String id = request.getParameter(BookField.ID).trim();
        String image = request.getParameter(BookField.IMAGE).trim();
        String lastImage = request.getParameter(LAST_IMAGE_KEY).trim();
        if (title == null || author == null || genre == null || size == null || count == null
                || annotation == null || id == null || lastImage == null || image == null) {
            throw new CommandException("Validation Exception");
        }
        if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || size.isEmpty()
                || count.isEmpty() || annotation.isEmpty() || id.isEmpty() || lastImage.isEmpty()) {
            throw new CommandException("Validation exception");
        }
        Book book = new Book();
        book.setId(Integer.parseInt(id));
        book.setTitle(title);
        book.setAuthor(author);
        book.setGenre(genre);
        book.setSize(Integer.parseInt(size));
        book.setAnnotation(annotation);
        if (image.isEmpty()) {
            book.setImage(lastImage);
        } else {
            book.setImage(getImage(request));
        }
        BookService bookService = BookService.getInstance();
        BookRecordService bookRecordService = BookRecordService.getInstance();
        try {
            bookService.update(book.getId(), book);
            BookRecordTO bookRecordTO = new BookRecordTO();
            bookRecordTO.setFreeCount(new Integer(count));
            bookRecordTO.setTotalCount(new Integer(count));
            bookRecordTO.setId_book(book.getId());
            BookRecordVO bookRecordVO = bookRecordService.readByBookId(book.getId());
            bookRecordService.update(bookRecordVO.getId(), bookRecordTO);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return MAIN_PAGE_COMMAND_KEY;
    }

    /**
     * <p>This method is used to download a image to the server from request</p>
     *
     * @param request is http request sent from the client
     * @return the path to saved image
     * @throws CommandException
     */
    private String getImage(HttpServletRequest request) throws CommandException {
        Part filePart;
        File imageFile;
        try {
            filePart = request.getPart(BookField.IMAGE);
            imageFile = new File(request.getServletContext().getRealPath("") + DIRECTION + System.currentTimeMillis() + EXPANSION);
        } catch (IOException | ServletException e) {
            throw new CommandException("Exception in parse Image", e);
        }
        try (InputStream inputStream = filePart.getInputStream();
             OutputStream outputStream = new FileOutputStream(imageFile)) {

            int read;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            throw new CommandException("Exception in parse Image", e);
        }
        return imageFile.getName();
    }
}