package by.epam.task6.controller.command.exception;

/**
 * <p>This exception throws when the error occurred on the server</p>
 * @author Nikita Kobyzov
 */
public class CommandException extends Exception {

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }
}
