package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.BookField;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to delete book from the data source</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class DeleteBook implements ICommand {
    private static final String MAIN_PAGE_COMMAND_KEY = "libraryController?ACTION=MAIN_PAGE";
    private static final String RESPONSE_KEY = "response";
    private static final String OK_MESSAGE_KEY = "deleteOk";
    private static final String BAD_MESSAGE_KEY = "deleteBad";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String id = request.getParameter(BookField.ID);
        if (id == null) {
            throw new CommandException("ValidationException");
        }
        try {
            BookService bookService = BookService.getInstance();
            boolean isDeleted = bookService.delete(Integer.parseInt(id));
            if (isDeleted) {
                request.setAttribute(RESPONSE_KEY, OK_MESSAGE_KEY);
            } else {
                request.setAttribute(RESPONSE_KEY, BAD_MESSAGE_KEY);
            }
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return MAIN_PAGE_COMMAND_KEY;
    }
}
