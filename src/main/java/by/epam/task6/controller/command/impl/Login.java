package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.fieldhelper.UserField;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used for user authorization</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class Login  implements ICommand {
    private static final Logger LOG = LogManager.getLogger(Login.class);
    private static final String PAGE = "index.jsp";
    private static final String USER_KEY = "user";
    private static final String RESPONSE_KEY = "response";
    private static final String OK_MESSAGE_KEY = "loginOk";
    private static final String BAD_MESSAGE_KEY = "loginBad";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String login = request.getParameter(UserField.LOGIN);
        String password = request.getParameter(UserField.PASSWORD);
        if (login == null || password == null) {
            request.setAttribute(RESPONSE_KEY, BAD_MESSAGE_KEY);
            return PAGE;
        }
        if (login.isEmpty() || password.isEmpty()) {
            request.setAttribute(RESPONSE_KEY, BAD_MESSAGE_KEY);
            return PAGE;
        }
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        UserService userService = UserService.getInstance();
        User responseUser;
        try {
            responseUser = userService.login(user);
        } catch (ClientServiceException e) {
            LOG.warn("Client error", e);
            request.setAttribute(RESPONSE_KEY, BAD_MESSAGE_KEY);
            return PAGE;
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        if (responseUser != null) {
            HttpSession session = request.getSession(true);
            session.setAttribute(USER_KEY, responseUser);
            request.setAttribute(RESPONSE_KEY, OK_MESSAGE_KEY);
        } else {
            request.setAttribute(RESPONSE_KEY, BAD_MESSAGE_KEY);
        }
        return PAGE;

    }
}
