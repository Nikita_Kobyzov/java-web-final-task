package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.vo.CartVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.CartService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>This class is used to forward to the page with user cart</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class UserCart implements ICommand {
    private static final String USER_KEY = "user";
    private static final String CART_LIST_KEY = "cartList";
    private static final String PAGE = "/WEB-INF/jsp/cart.jsp";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_KEY);
        try {
            CartService cartService = CartService.getInstance();
            List<CartVO> cartVOList = cartService.readByUserID(user.getId());
            request.setAttribute(CART_LIST_KEY, cartVOList);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return PAGE;
    }
}
