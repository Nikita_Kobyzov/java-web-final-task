package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.vo.BackList;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to forward to the page with book catalog</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class MainPage implements ICommand {
    private static final String BACKLIST_KEY = "backlist";
    private static final String PAGE = "/WEB-INF/jsp/main.jsp";
    private static final String USER_KEY = "user";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        UserStatus userStatus;
        HttpSession session = request.getSession(false);
        if (session == null) {
            userStatus = UserStatus.NONE;
        } else {
            if (session.getAttribute(USER_KEY) == null) {
                userStatus = UserStatus.NONE;
            } else {
                User user = (User) session.getAttribute(USER_KEY);
                if (user.getUserStatus().equals(UserStatus.ADMIN)) {
                    userStatus = UserStatus.ADMIN;
                } else {
                    userStatus = UserStatus.CLIENT;
                }
            }
        }
        BackList backList;
        try {
            BookRecordService bookRecordService = BookRecordService.getInstance();
            if (userStatus.equals(UserStatus.ADMIN)) {
                backList = bookRecordService.getBackListForAdmin();
            } else {
                backList = bookRecordService.getBackList();
            }
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        request.setAttribute(BACKLIST_KEY, backList);
        return PAGE;
    }
}
