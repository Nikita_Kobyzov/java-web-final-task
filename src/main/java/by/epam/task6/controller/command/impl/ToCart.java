package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.to.CartTO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.CartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to put book to user cart</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class ToCart implements ICommand {
    private static final String USER_KEY = "user";
    private static final String ID_BOOK_KEY = "BOOK_ID";
    private static final String MAIN_PAGE_COMMAND_KEY = "libraryController?ACTION=MAIN_PAGE";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_KEY);
        String idBook = request.getParameter(ID_BOOK_KEY);
        if (idBook == null) {
            throw new CommandException("Validation exception");
        }
        CartTO cartTO = new CartTO();
        cartTO.setId_book(new Integer(idBook));
        cartTO.setId_client(user.getId());
        CartService cartService = CartService.getInstance();
        try {
            cartService.write(cartTO);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return MAIN_PAGE_COMMAND_KEY;
    }
}
