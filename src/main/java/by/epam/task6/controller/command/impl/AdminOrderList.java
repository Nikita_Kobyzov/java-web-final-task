package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.domain.vo.OrderVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.OrderService;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>This class is used to forward to the page with user's order list</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class AdminOrderList implements ICommand {
    private static final String CONFIRM_ORDER_LIST_KEY = "confirmOrders";
    private static final String NO_CONFIRM_ORDER_LIST_KEY = "noConfirmOrders";
    private static final String ADMIN_PAGE = "/WEB-INF/jsp/given_books.jsp";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            OrderService orderService = OrderService.getInstance();
            List<OrderVO> confirmVOList;
            confirmVOList = orderService.readAllConfirmed();
            request.setAttribute(CONFIRM_ORDER_LIST_KEY, confirmVOList);
            List<OrderVO> noConfirmVOList;
            noConfirmVOList = orderService.readAllNotConfirmed();
            request.setAttribute(NO_CONFIRM_ORDER_LIST_KEY, noConfirmVOList);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return ADMIN_PAGE;
    }
}
