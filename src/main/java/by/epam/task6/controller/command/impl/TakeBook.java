package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.OrderField;
import by.epam.task6.domain.to.OrderTO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.CartService;
import by.epam.task6.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to create a new order</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class TakeBook implements ICommand {
    private static final String CART_ID_KEY = "CART_ID";
    private static final String BOOK_ID_KEY = "BOOK_ID";
    private static final String USER_KEY = "user";
    private static final Boolean DEFAULT_CONFORMATION = false;
    private static final String PAGE = "libraryController?ACTION=USER_CART";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_KEY);
        String cartID = request.getParameter(CART_ID_KEY);
        String bookID = request.getParameter(BOOK_ID_KEY);
        String readingPlace = request.getParameter(OrderField.READING_PLACE);
        if (cartID == null || bookID == null || readingPlace == null) {
            throw new CommandException("Validation exception");
        }
        if (cartID.isEmpty() || bookID.isEmpty() || readingPlace.isEmpty()) {
            throw new CommandException("Validation exception");
        }
        OrderTO orderTO = new OrderTO();
        orderTO.setId_client(user.getId());
        orderTO.setId_book(new Integer(bookID));
        orderTO.setConfirmation(DEFAULT_CONFORMATION);
        orderTO.setReadingPlace(readingPlace);
        CartService cartService = CartService.getInstance();
        OrderService orderService = OrderService.getInstance();
        try {
            orderService.write(orderTO);
            cartService.delete(new Integer(cartID));
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return PAGE;
    }
}
