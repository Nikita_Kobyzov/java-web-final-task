package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.OrderField;
import by.epam.task6.domain.to.OrderTO;
import by.epam.task6.domain.vo.OrderVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to confirm a user's order</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class ConfirmOrder implements ICommand {
    private static boolean CONFORMATION_VALUE=true;
    private static final String PAGE="libraryController?ACTION=ADMIN_ORDER_LIST";
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String id=request.getParameter(OrderField.ID);
        if(id==null){
            throw new CommandException("Validation exception");
        }
        OrderService orderService=OrderService.getInstance();
        try{
            OrderVO orderVO=orderService.read(Integer.parseInt(id));
            OrderTO orderTO=new OrderTO();
            orderTO.setId_book(orderVO.getBook().getId());
            orderTO.setReadingPlace(orderVO.getReadingPlace());
            orderTO.setConfirmation(CONFORMATION_VALUE);
            orderTO.setId_client(orderVO.getUser().getId());
            orderService.update(Integer.parseInt(id),orderTO);
        }catch (ServiceException e){
            throw new CommandException("Exception in command",e);
        }
        return PAGE;
    }
}
