package by.epam.task6.controller.command;

import by.epam.task6.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This interface describes the basic method of the processing requests.This
 * interface implements a pattern <strong>Command</strong></p>
 * @see by.epam.task6.controller.CommandFactory
 * @author Nikita Kobyzov
 */
public interface ICommand {
    /**
     * <p>This method process http request</p>
     *
     * @param request is http request sent from the client
     * @return the url
     * @throws CommandException if the error occurred on the server
     */
    String execute(HttpServletRequest request) throws CommandException;
}
