package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.OrderField;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.vo.BookRecordVO;
import by.epam.task6.domain.vo.OrderVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;
import by.epam.task6.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to delete order from the data source</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class DeleteOrder implements ICommand {
    private static final String PAGE = "libraryController?ACTION=ADMIN_ORDER_LIST";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String id = request.getParameter(OrderField.ID);
        if (id == null) {
            throw new CommandException("Validation exception");
        }
        OrderService orderService = OrderService.getInstance();
        BookRecordService bookRecordService = BookRecordService.getInstance();
        try {
            OrderVO orderVO = orderService.read(Integer.parseInt(id));
            orderService.delete(Integer.parseInt(id));
            BookRecordVO bookRecordVO = bookRecordService.readByBookId(orderVO.getBook().getId());
            BookRecordTO bookRecordTO = new BookRecordTO();
            bookRecordTO.setFreeCount(bookRecordVO.getFreeCount() + 1);
            bookRecordTO.setTotalCount(bookRecordVO.getTotalCount());
            bookRecordTO.setId_book(bookRecordVO.getBook().getId());
            bookRecordService.update(bookRecordVO.getId(), bookRecordTO);
        } catch (ServiceException e) {
            throw new CommandException("Exception in command", e);
        }
        return PAGE;
    }
}
