package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.CommandFactory;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to change language on pages</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class ChangeLanguage implements ICommand {
    private static final String LANGUAGE = "LANGUAGE";
    private static final String PAGE = "PAGE";
    private static final String LAST_COMMAND = "LAST_COMMAND";
    private static final String SESSION_LANG_KEY = "lang";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String lang = request.getParameter(LANGUAGE);
        String page = request.getParameter(PAGE);
        String lastCommand = request.getParameter(LAST_COMMAND);
        if (lang == null || (page == null && lastCommand == null)) {
            throw new CommandException("Validation Exception");
        }
        HttpSession session = request.getSession(true);
        session.setAttribute(SESSION_LANG_KEY, lang);
        if (lastCommand == null) {
            return page;
        }
        CommandFactory commandFactory = CommandFactory.getInstance();
        ICommand command = commandFactory.getCommand(lastCommand);
        return command.execute(request);
    }
}