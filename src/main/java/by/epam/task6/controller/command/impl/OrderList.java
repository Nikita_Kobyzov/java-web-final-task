package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.vo.OrderVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>This class is used to forward to the page with user's order list</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class OrderList implements ICommand {
    private static final String USER_KEY = "user";
    private static final String ORDER_LIST_KEY = "orderList";
    private static final String CLIENT_PAGE = "/WEB-INF/jsp/taken_books.jsp";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(USER_KEY);
        try {
            OrderService orderService = OrderService.getInstance();
            List<OrderVO> orderVOList;
            orderVOList = orderService.readAllByUserId(user.getId());
            request.setAttribute(ORDER_LIST_KEY, orderVOList);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return CLIENT_PAGE;

    }
}
