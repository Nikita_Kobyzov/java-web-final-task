package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.fieldhelper.BookField;
import by.epam.task6.domain.fieldhelper.BookRecordField;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;
import by.epam.task6.service.impl.BookService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.*;

/**
 * <p>This class is used to save a new book to the data source</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class AddBook implements ICommand {
    private static final Logger LOG = LogManager.getLogger(AddBook.class);
    private static final String MAIN_PAGE_COMMAND_KEY = "libraryController?ACTION=MAIN_PAGE";
    private static final String DIRECTION = "image\\book\\";
    private static final String EXPANSION = ".jpeg";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String title = request.getParameter(BookField.TITLE).trim();
        String author = request.getParameter(BookField.AUTHOR).trim();
        String genre = request.getParameter(BookField.GENRE).trim();
        String size = request.getParameter(BookField.SIZE).trim();
        String count = request.getParameter(BookRecordField.FREE_COUNT).trim();
        String annotation = request.getParameter(BookField.ANNOTATION).trim();
        if (title == null || author == null || genre == null || size == null || count == null
                || annotation == null) {
            LOG.warn("Not enough of parameters");
            return MAIN_PAGE_COMMAND_KEY;
        }
        if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || size.isEmpty()
                || count.isEmpty() || annotation.isEmpty()) {
            LOG.warn("Not enough of parameters");
            return MAIN_PAGE_COMMAND_KEY;
        }
        Book book = new Book();
        book.setTitle(title);
        book.setAuthor(author);
        book.setGenre(genre);
        book.setSize(new Integer(size));
        book.setImage(getImage(request));
        book.setAnnotation(annotation);
        BookService bookService = BookService.getInstance();
        BookRecordService bookRecordService = BookRecordService.getInstance();
        try {
            Book bookVO = bookService.write(book);
            BookRecordTO bookRecordTO = new BookRecordTO();
            bookRecordTO.setFreeCount(Integer.parseInt(count));
            bookRecordTO.setTotalCount(Integer.parseInt(count));
            bookRecordTO.setId_book(bookVO.getId());
            bookRecordService.write(bookRecordTO);
        } catch (ClientServiceException e) {
            LOG.warn("Client error", e);
            return MAIN_PAGE_COMMAND_KEY;
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return MAIN_PAGE_COMMAND_KEY;
    }

    /**
     * <p>This method is used to download a image to the server from request</p>
     *
     * @param request is http request sent from the client
     * @return the path to saved image
     * @throws CommandException
     */
    private String getImage(HttpServletRequest request) throws CommandException {
        Part filePart;
        File imageFile;
        try {
            filePart = request.getPart(BookField.IMAGE);
            imageFile = new File(request.getServletContext().getRealPath("")
                    + DIRECTION + System.currentTimeMillis() + EXPANSION);
        } catch (IOException | ServletException e) {
            throw new CommandException("Exception in parse Image", e);
        }
        try (InputStream inputStream = filePart.getInputStream();
             OutputStream outputStream = new FileOutputStream(imageFile)) {
            int read;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            throw new CommandException("Exception in parse Image", e);
        }
        return imageFile.getName();
    }
}