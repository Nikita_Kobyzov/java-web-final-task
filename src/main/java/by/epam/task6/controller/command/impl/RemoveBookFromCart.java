package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.vo.BookRecordVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;
import by.epam.task6.service.impl.CartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to remove book from user cart</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class RemoveBookFromCart implements ICommand {
    private static final String CART_ID_KEY = "CART_ID";
    private static final String BOOK_ID_KEY = "BOOK_ID";
    private static final String PAGE = "libraryController?ACTION=USER_CART";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String cartID = request.getParameter(CART_ID_KEY);
        String bookID = request.getParameter(BOOK_ID_KEY);
        if (cartID == null || bookID == null) {
            throw new CommandException("Validation exception");
        }
        if (cartID.isEmpty() || bookID.isEmpty()) {
            throw new CommandException("Validation exception");
        }
        BookRecordService bookRecordService = BookRecordService.getInstance();
        CartService cartService = CartService.getInstance();
        try {
            cartService.delete(new Integer(cartID));
            BookRecordVO bookRecordVO = bookRecordService.readByBookId(new Integer(bookID));
            BookRecordTO bookRecordTO = new BookRecordTO();
            bookRecordTO.setId(bookRecordVO.getId());
            bookRecordTO.setFreeCount(bookRecordVO.getFreeCount() + 1);
            bookRecordTO.setTotalCount(bookRecordVO.getTotalCount());
            bookRecordTO.setId_book(bookRecordVO.getBook().getId());
            bookRecordService.update(bookRecordVO.getId(), bookRecordTO);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return PAGE;
    }
}