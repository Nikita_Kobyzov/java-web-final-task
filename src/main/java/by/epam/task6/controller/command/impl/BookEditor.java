package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.domain.vo.BookRecordVO;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookRecordService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to forward to the editor page</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class BookEditor implements ICommand {
    private static final String ID_KEY = "ID";
    private static final String BOOK_RECORD_KEY = "book_record";
    private static final String PAGE = "/WEB-INF/jsp/book_editor.jsp";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String id = request.getParameter(ID_KEY);
        try {
            BookRecordService bookRecordService = BookRecordService.getInstance();
            BookRecordVO bookRecordVO = bookRecordService.read(new Integer(id));
            request.setAttribute(BOOK_RECORD_KEY, bookRecordVO);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Command", e);
        }
        return PAGE;
    }
}
