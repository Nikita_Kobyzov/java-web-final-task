package by.epam.task6.controller.command.impl;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.fieldhelper.BookField;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.impl.BookService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is used to get a book on a book unique identifier</p>
 * @see by.epam.task6.controller.command.ICommand
 * @author Nikita Kobyzov
 */
public class BookView implements ICommand {
    private static final String PAGE = "/WEB-INF/jsp/bookview.jsp";
    private static final String BOOK_KEY = "book";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String id =request.getParameter(BookField.ID);
        if (id == null) {
            throw new CommandException("Validation Exception");
        }
        try {
            BookService bookService = BookService.getInstance();
            Book book = bookService.read(Integer.parseInt(id));
            if (book == null) {
                throw new CommandException("Bad Request");
            }
            request.setAttribute(BOOK_KEY, book);
        } catch (ServiceException e) {
            throw new CommandException("Exception in command", e);
        }
        return PAGE;
    }
}
