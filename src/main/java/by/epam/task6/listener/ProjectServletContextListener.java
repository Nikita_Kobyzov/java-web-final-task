package by.epam.task6.listener;

import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * <p>This class of handler the servlet context events</p>
 * @author Nikita Kobyzov
 */
public class ProjectServletContextListener implements ServletContextListener {
    private static final Logger LOG = LogManager.getLogger(ProjectServletContextListener.class);
    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            connectionPool.init();
        } catch (ConnectionPoolException e) {
            LOG.error("Error in connection pool initialize", e);
            throw new RuntimeException("Error in connection pool initialize", e);
        }
        LOG.info("Servlet is initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            connectionPool.destroy();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Can't close a connection pool");
        }
        LOG.info("Servlet is destroyed");
    }
}
