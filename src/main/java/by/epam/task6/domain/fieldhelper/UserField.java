package by.epam.task6.domain.fieldhelper;

/**
 * <p>This class contains the names of the users field</p>
 * @author Nikita Kobyzov
 */
public final class UserField {
    public static final String LOGIN = "LOGIN";
    public static final String ID = "ID";
    public static final String PASSWORD = "PASSWORD";
    public static final String NUMBER = "NUMBER";
    public static final String FNAME = "FNAME";
    public static final String LNAME = "LNAME";
    public static final String USER_STATUS = "USER_STATUS";
}
