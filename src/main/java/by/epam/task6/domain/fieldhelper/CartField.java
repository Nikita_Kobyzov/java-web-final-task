package by.epam.task6.domain.fieldhelper;

/**
 * <p>This class contains the names of the carts field</p>
 * @author Nikita Kobyzov
 */
public final class CartField {
    public static final String ID = "ID";
    public static final String ID_CLIENT = "ID_CLIENT";
    public static final String ID_BOOK = "ID_BOOK";
}
