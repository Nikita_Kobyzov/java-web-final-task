package by.epam.task6.domain.fieldhelper;

/**
 * <p>This class contains the names of the orders field</p>
 * @author Nikita Kobyzov
 */
public final class OrderField {
    public static final String ID = "ID";
    public static final String ID_CLIENT = "ID_CLIENT";
    public static final String ID_BOOK = "ID_BOOK";
    public static final String CONFIRMATION = "CONFIRMATION";
    public static final String READING_PLACE = "READING_PLACE";
}
