package by.epam.task6.domain.fieldhelper;

/**
 * <p>This class contains the names of the books field</p>
 * @author Nikita Kobyzov
 */
public final class BookField {
    public static final String ID = "ID";
    public static final String TITLE = "TITLE";
    public static final String AUTHOR = "AUTHOR";
    public static final String SIZE = "SIZE";
    public static final String GENRE = "GENRE";
    public static final String IMAGE = "IMAGE";
    public static final String ANNOTATION = "ANNOTATION";
}
