package by.epam.task6.domain.fieldhelper;

/**
 * <p>This class contains the names of the book records field</p>
 * @author Nikita Kobyzov
 */
public class BookRecordField {
    public static final String ID = "ID";
    public static final String ID_BOOK = "ID_BOOK";
    public static final String TOTAL_COUNT = "TOTAL_COUNT";
    public static final String FREE_COUNT = "FREE_COUNT";
}
