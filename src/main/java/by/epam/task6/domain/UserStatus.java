package by.epam.task6.domain;

/**
 * <p>This enum contains the status of users</p>
 * @author Nikita Kobyzov
 */
public enum UserStatus {
     CLIENT, ADMIN, NONE
}
