package by.epam.task6.domain;


/**
 * <p>This class describes the User-entity</p>
 * @author Nikita Kobyzov
 */
public class User {
    private Integer id;
    private String login;
    private String password;
    private Integer number;
    private String fName;
    private String lName;
    private UserStatus userStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) {
            return false;
        }
        if (login != null ? !login.equals(user.login) : user.login != null) {
            return false;
        }
        if (password != null ? !password.equals(user.password) : user.password != null) {
            return false;
        }
        if (number != null ? !number.equals(user.number) : user.number != null) {
            return false;
        }
        if (fName != null ? !fName.equals(user.fName) : user.fName != null) {
            return false;
        }
        if (lName != null ? !lName.equals(user.lName) : user.lName != null) {
            return false;
        }
        if(userStatus != null ? !userStatus.equals(user.getUserStatus()) : user.userStatus !=null) {
            return false;
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (fName != null ? fName.hashCode() : 0);
        result = 31 * result + (lName != null ? lName.hashCode() : 0);
        result = 31 * result + (userStatus != null ? userStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName()+"{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", number=" + number +
                ", fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", userStatus=" + userStatus +
                '}';
    }
}
