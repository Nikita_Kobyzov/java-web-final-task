package by.epam.task6.domain.to;

/**
 * <p>This class describes the BookRecord-entity. It has unique identifier
 * of the book</p>
 * @author Nikita Kobyzov
 */
public class BookRecordTO {
    private Integer id;
    private Integer id_book;
    private Integer totalCount;
    private Integer freeCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_book() {
        return id_book;
    }

    public void setId_book(Integer id_book) {
        this.id_book = id_book;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(Integer freeCount) {
        this.freeCount = freeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookRecordTO that = (BookRecordTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (id_book != null ? !id_book.equals(that.id_book) : that.id_book != null) {
            return false;
        }
        if (totalCount != null ? !totalCount.equals(that.totalCount) :
                that.totalCount != null) {
            return false;
        }
        if (freeCount != null ? !freeCount.equals(that.freeCount) :
                that.freeCount != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (id_book != null ? id_book.hashCode() : 0);
        result = 31 * result + (totalCount != null ? totalCount.hashCode() : 0);
        result = 31 * result + (freeCount != null ? freeCount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "id=" + id +
                ", id_book=" + id_book +
                ", totalCount=" + totalCount +
                ", freeCount=" + freeCount +
                '}';
    }
}
