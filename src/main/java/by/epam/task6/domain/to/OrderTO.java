package by.epam.task6.domain.to;

/**
 * <p>This class describes the BookRecord-entity. It has unique identifiers
 * of the book and user</p>
 * @author Nikita Kobyzov
 */
public class OrderTO {
    private Integer id;
    private Integer id_client;
    private Integer id_book;
    private Boolean confirmation;
    private String readingPlace;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public int getId_book() {
        return id_book;
    }

    public void setId_book(int id_book) {
        this.id_book = id_book;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public String getReadingPlace() {
        return readingPlace;
    }

    public void setReadingPlace(String readingPlace) {
        this.readingPlace = readingPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderTO orderTO = (OrderTO) o;

        if (id != null ? !id.equals(orderTO.id) : orderTO.id != null) {
            return false;
        }
        if (id_client != null ? !id_client.equals(orderTO.id_client) :
                orderTO.id_client != null) {
            return false;
        }
        if (id_book != null ? !id_book.equals(orderTO.id_book) :
                orderTO.id_book != null) {
            return false;
        }
        if (confirmation != null ? !confirmation.equals(orderTO.confirmation) :
                orderTO.confirmation != null){
            return false;
        }

        if (readingPlace != null ? !readingPlace.equals(orderTO.readingPlace) :
                orderTO.readingPlace != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (id_client != null ? id_client.hashCode() : 0);
        result = 31 * result + (id_book != null ? id_book.hashCode() : 0);
        result = 31 * result + (confirmation != null ? confirmation.hashCode() : 0);
        result = 31 * result + (readingPlace != null ? readingPlace.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "" +
                "id=" + id +
                ", id_client=" + id_client +
                ", id_book=" + id_book +
                ", confirmation=" + confirmation +
                ", readingPlace='" + readingPlace + '\'' +
                '}';
    }
}
