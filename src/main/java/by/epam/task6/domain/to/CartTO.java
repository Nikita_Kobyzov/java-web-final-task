package by.epam.task6.domain.to;

/**
 * <p>This class describes the Cart-entity. It has unique identifiers
 * of the book and user</p>
 * @author Nikita Kobyzov
 */
public class CartTO {
    private Integer id;
    private Integer id_client;
    private Integer id_book;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public int getId_book() {
        return id_book;
    }

    public void setId_book(int id_book) {
        this.id_book = id_book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CartTO cartTO = (CartTO) o;

        if (id != null ? !id.equals(cartTO.id) : cartTO.id != null) {
            return false;
        }
        if (id_client != null ? !id_client.equals(cartTO.id_client) :
                cartTO.id_client != null) {
            return false;
        }
        if (id_book != null ? !id_book.equals(cartTO.id_book) :
                cartTO.id_book != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (id_client != null ? id_client.hashCode() : 0);
        result = 31 * result + (id_book != null ? id_book.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "id=" + id +
                ", id_client=" + id_client +
                ", id_book=" + id_book +
                '}';
    }
}
