package by.epam.task6.domain.vo;

import by.epam.task6.domain.Book;

/**
 * <p>This class describes the BookRecord-entity</p>
 * @author Nikita Kobyzov
 */
public class BookRecordVO {
    private Integer id;
    private Book book;
    private Integer totalCount;
    private Integer freeCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(Integer freeCount) {
        this.freeCount = freeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookRecordVO that = (BookRecordVO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (book != null ? !book.equals(that.book) : that.book != null) {
            return false;
        }
        if (totalCount != null ? !totalCount.equals(that.totalCount) : that.totalCount != null) {
            return false;
        }
        if (freeCount != null ? !freeCount.equals(that.freeCount) : that.freeCount != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (book != null ? book.hashCode() : 0);
        result = 31 * result + (totalCount != null ? totalCount.hashCode() : 0);
        result = 31 * result + (freeCount != null ? freeCount.hashCode() : 0);
        return result;
    }
}
