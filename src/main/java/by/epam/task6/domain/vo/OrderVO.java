package by.epam.task6.domain.vo;

import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;

/**
 * <p>This class describes the Order-entity</p>
 * @author Nikita Kobyzov
 */
public class OrderVO {
    private Integer id;
    private User user;
    private Book book;
    private Boolean confirmation;
    private String readingPlace;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public String getReadingPlace() {
        return readingPlace;
    }

    public void setReadingPlace(String readingPlace) {
        this.readingPlace = readingPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderVO orderVO = (OrderVO) o;

        if (id != null ? !id.equals(orderVO.id) : orderVO.id != null) {
            return false;
        }
        if (user != null ? !user.equals(orderVO.user) : orderVO.user != null) {
            return false;
        }
        if (book != null ? !book.equals(orderVO.book) : orderVO.book != null) {
            return false;
        }
        if (confirmation != null ? !confirmation.equals(orderVO.confirmation) :
                orderVO.confirmation != null){
            return false;
        }
        if (readingPlace != null ? !readingPlace.equals(orderVO.readingPlace) :
                orderVO.readingPlace != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        result = 31 * result + (confirmation != null ? confirmation.hashCode() : 0);
        result = 31 * result + (readingPlace != null ? readingPlace.hashCode() : 0);
        return result;
    }
}
