package by.epam.task6.domain.vo;
import java.util.List;

/**
 * <p>This class describes the BackList-entity. It has a list of the pages,
 * which is used for pagination</p>
 * @author Nikita Kobyzov
 */
public class BackList {
    private List<Page> pageList;

    public List<Page> getPageList() {
        return pageList;
    }

    public void setPageList(List<Page> pageList) {
        this.pageList = pageList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BackList that = (BackList) o;

        if(pageList != null ? !pageList.equals(that.pageList) : that.pageList != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return pageList != null ? pageList.hashCode() : 0;
    }
}
