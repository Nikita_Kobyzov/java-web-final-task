package by.epam.task6.domain.vo;

import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;

/**
 * <p>This class describes the Cart-entity</p>
 * @author Nikita Kobyzov
 */
public class CartVO {
    private Integer id;
    private User user;
    private Book book;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CartVO cartVO = (CartVO) o;

        if (id != null ? !id.equals(cartVO.id) : cartVO.id != null) {
            return false;
        }
        if (user != null ? !user.equals(cartVO.user) : cartVO.user != null) {
            return false;
        }
        if (book != null ? !book.equals(cartVO.book) : cartVO.book != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        return result;
    }
}