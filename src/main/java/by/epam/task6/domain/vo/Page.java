package by.epam.task6.domain.vo;

import java.util.List;

/**
 * <p>This class is used for pagination. It has a list of book records</p>
 * @see by.epam.task6.domain.vo.BackList
 * @author Nikita Kobyzov
 */
public class Page {
    private List<BookRecordVO> bookRecordVOList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;

    public List<BookRecordVO> getBookRecordVOList() {
        return bookRecordVOList;
    }

    public void setBookRecordVOList(List<BookRecordVO> bookRecordVOList) {
        this.bookRecordVOList = bookRecordVOList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Page page = (Page) o;
        if (bookRecordVOList != null ? !bookRecordVOList.equals(page.bookRecordVOList) :
                page.bookRecordVOList != null){
            return false;
        }
        if (id != null ? !id.equals(page.id) : page.id != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = bookRecordVOList != null ? bookRecordVOList.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
