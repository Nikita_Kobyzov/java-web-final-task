package by.epam.task6.service.exception;

/**
 * <p>This exception throws when the error has occurred in the business logic.
 * The reason for the error occurred on the server side</p>
 * @author Nikita Kobyzov
 */
public class ServerServiceException extends ServiceException {
    public ServerServiceException() {
        super();
    }

    public ServerServiceException(String message) {
        super(message);
    }

    public ServerServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerServiceException(Throwable cause) {
        super(cause);
    }
}