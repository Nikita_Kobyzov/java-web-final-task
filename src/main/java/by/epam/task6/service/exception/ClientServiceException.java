package by.epam.task6.service.exception;

/**
 * <p>This exception throws when the error has occurred in the business logic.
 * The reason for the error occurred on the client side</p>
 * @author Nikita Kobyzov
 */
public class ClientServiceException extends ServiceException {
    public ClientServiceException() {
        super();
    }

    public ClientServiceException(String message) {
        super(message);
    }

    public ClientServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientServiceException(Throwable cause) {
        super(cause);
    }
}
