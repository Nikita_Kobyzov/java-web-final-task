package by.epam.task6.service.exception;

/**
 * <p>This exception throws when the error has occurred in the business logic</p>
 * @author Nikita Kobyzov
 */
public class ServiceException extends Exception {

    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
