package by.epam.task6.service;

import by.epam.task6.service.exception.ServiceException;

/**
 * <p>This interface describes that classes which implements this
 * interface have the ability to perform the delete operation</p>
 * @param <K> describes a unique identifier of the deleted entity
 * @author Nikita Kobyzov
 */
public interface Deleting<K> {
    /**
     * <p>This method delete an entity on a key</p>
     *
     * @param key is a unique identifier of the entity that to be deleted
     * @return <tt>true</tt> is the object was deleted
     * @throws ServiceException if an error has occurred with the removal operations
     */
    boolean delete(K key) throws ServiceException;
}
