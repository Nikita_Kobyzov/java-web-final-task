package by.epam.task6.service.impl;

import by.epam.task6.dao.*;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.to.CartTO;
import by.epam.task6.domain.vo.CartVO;
import by.epam.task6.service.*;
import by.epam.task6.service.Readable;
import by.epam.task6.service.exception.ServerServiceException;
import by.epam.task6.service.exception.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is designed to solve problems of business logic for
 * a Cart-entity</p>
 * @author Nikita Kobyzov
 */
public class CartService implements Writable<CartTO, CartVO>, Readable<Integer, CartVO>,
        Deleting<Integer> {
    private static final CartService INSTANCE = new CartService();
    private static final Lock LOCK = new ReentrantLock();

    private CartService() {
    }

    public static CartService getInstance() {
        return INSTANCE;
    }

    @Override
    public CartVO write(CartTO entity) throws ServiceException {
        CartVO cartVO;
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            CartDao cartDao = daoFactory.getCartDao();
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            LOCK.lock();
            Book book = new Book();
            book.setId(entity.getId_book());
            BookRecordTO bookRecordTO = bookRecordDao.readByBookId(book.getId());
            if (bookRecordTO == null) {
                throw new ServiceException("Validation exception");
            }
            if (bookRecordTO.getFreeCount() <= 0) {
                throw new ServiceException("Illegal operation");
            }
            int newFreeCount = bookRecordTO.getFreeCount() - 1;
            bookRecordTO.setFreeCount(newFreeCount);
            bookRecordDao.update(bookRecordTO.getId(), bookRecordTO);
            CartTO cartTO = cartDao.create(entity);
            cartVO = new CartVO();
            cartVO.setId(cartTO.getId());
            BookDao bookDao = daoFactory.getBookDao();
            book = bookDao.read(entity.getId_book());
            cartVO.setBook(book);
            UserDao userDao = daoFactory.getUserDao();
            User user = userDao.read(entity.getId_client());
            cartVO.setUser(user);
        } catch (DaoException e) {
            throw new ServiceException("Exception in Cart Service", e);
        } finally {
            LOCK.unlock();
        }
        return cartVO;

    }

    @Override
    public List<CartVO> readAll() throws ServiceException {
        List<CartVO> cartVOList = new ArrayList<CartVO>();
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            CartDao cartDao = daoFactory.getCartDao();
            UserDao userDao = daoFactory.getUserDao();
            BookDao bookDao = daoFactory.getBookDao();
            List<CartTO> cartTOList = cartDao.read();
            for (CartTO cartTO : cartTOList) {
                CartVO cartVO = new CartVO();
                cartVO.setId(cartTO.getId());
                cartVO.setBook(bookDao.read(cartTO.getId_book()));
                cartVO.setUser(userDao.read(cartTO.getId_client()));
                cartVOList.add(cartVO);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in Cart Service", e);
        }
        return cartVOList;
    }

    /**
     * <p>This method reads the cart records on a user unique identifier</p>
     *
     * @param id is a unique identifier of the user
     * @return a list of carts corresponding to the given user id
     * @throws ServiceException if an error has occurred in the business logic
     */
    public List<CartVO> readByUserID(Integer id) throws ServiceException {
        List<CartVO> cartVOList = new ArrayList<CartVO>();
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            CartDao cartDao = daoFactory.getCartDao();
            UserDao userDao = daoFactory.getUserDao();
            BookDao bookDao = daoFactory.getBookDao();
            List<CartTO> cartTOList = cartDao.readByUserId(id);
            for (CartTO cartTO : cartTOList) {
                CartVO cartVO = new CartVO();
                cartVO.setId(cartTO.getId());
                cartVO.setBook(bookDao.read(cartTO.getId_book()));
                cartVO.setUser(userDao.read(cartTO.getId_client()));
                cartVOList.add(cartVO);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in Cart Service", e);
        }
        return cartVOList;
    }

    @Override
    public CartVO read(Integer key) throws ServiceException {
        CartVO cartVO;
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            CartDao cartDao = daoFactory.getCartDao();
            UserDao userDao = daoFactory.getUserDao();
            BookDao bookDao = daoFactory.getBookDao();
            CartTO cartTO = cartDao.read(key);
            if (cartTO == null) {
                return null;
            }
            cartVO = new CartVO();
            cartVO.setId(cartTO.getId());
            cartVO.setBook(bookDao.read(cartTO.getId_book()));
            cartVO.setUser(userDao.read(cartTO.getId_client()));
        } catch (DaoException e) {
            throw new ServiceException("Exception in Cart Service", e);
        }
        return cartVO;
    }

    @Override
    public boolean delete(Integer key) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            CartDao cartDao = daoFactory.getCartDao();
            cartDao.delete(key);
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Cart Service");
        }
        return true;
    }
}