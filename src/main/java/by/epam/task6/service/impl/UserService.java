package by.epam.task6.service.impl;

import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.DaoFactory;
import by.epam.task6.dao.UserDao;
import by.epam.task6.domain.User;
import by.epam.task6.service.*;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServerServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.service.util.PasswordEncoder;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is designed to solve problems of business logic for
 * a User-entity</p>
 * @author Nikita Kobyzov
 */
public class UserService implements Writable<User, User> {
    private static final UserService INSTANCE = new UserService();
    private static final Lock MODIFY_LOCK = new ReentrantLock();
    private static final Validator validator = Validator.getInstance();

    private UserService() {
    }

    public static UserService getInstance() {
        return INSTANCE;
    }

    @Override
    public User write(User entity) throws ServiceException {
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        User result = null;
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            UserDao userDao = daoFactory.getUserDao();
            MODIFY_LOCK.lock();
            User dbUser = userDao.read(entity.getLogin());
            if (dbUser == null) {
                entity.setPassword(PasswordEncoder.encode(entity.getPassword()));
                result = userDao.create(entity);
            }
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in User Service", e);
        } finally {
            MODIFY_LOCK.unlock();
        }
        return result;
    }

    /**
     * <p>This method is used for user authorization</p>
     *
     * @param user is a person which try to pass authorization
     * @return the user, if he was authorized or <tt>null</tt> otherwise
     * @throws ServiceException if an error has occurred in the business logic
     */
    public User login(User user) throws ServiceException {
        try {
            validator.validate(user);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        User dbUser;
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            UserDao userDao = daoFactory.getUserDao();
            dbUser = userDao.read(user.getLogin());
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in User Service", e);
        }
        if (dbUser == null) {
            return null;
        }
        String password = user.getPassword();
        String encodePassword = PasswordEncoder.encode(password);
        if (dbUser.getPassword().equals(encodePassword)) {
            return dbUser;
        }
        return null;
    }
}