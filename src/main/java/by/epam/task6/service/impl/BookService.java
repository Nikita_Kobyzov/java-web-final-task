package by.epam.task6.service.impl;

import by.epam.task6.dao.BookDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.DaoFactory;
import by.epam.task6.domain.Book;
import by.epam.task6.service.*;
import by.epam.task6.service.Readable;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServerServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is designed to solve problems of business logic for
 * a Book-entity</p>
 * @author Nikita Kobyzov
 */
public class BookService implements Readable<Integer, Book>, Deleting<Integer>,Writable<Book, Book>,
        Updating<Integer, Book> {

    private static final Lock MODIFY_LOCK = new ReentrantLock();
    private static final BookService INSTANCE = new BookService();

    private BookService() {
    }

    public static BookService getInstance() {
        return INSTANCE;
    }

    @Override
    public Book write(Book entity) throws ServiceException {
        Validator validator = Validator.getInstance();
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        BookDao bookDao = daoFactory.getBookDao();
        try {
            Book bookVO = bookDao.create(entity);
            return bookVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Book Service", e);
        }
    }

    @Override
    public List<Book> readAll() throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            List<Book> bookList = bookDao.read();
            return bookList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Book Service", e);
        }
    }

    @Override
    public Book read(Integer key) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            Book book = bookDao.read(key);
            return book;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Book Service", e);
        }
    }

    @Override
    public boolean delete(Integer key) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            MODIFY_LOCK.lock();
            Book book = bookDao.read(key);
            if (book == null) {
                return false;
            }
            bookDao.delete(key);
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Book Service", e);
        }finally {
            MODIFY_LOCK.unlock();
        }
        return true;
    }

    @Override
    public boolean update(Integer key, Book entity) throws ServiceException {
        Validator validator = Validator.getInstance();
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            Book bookVO = bookDao.update(key, entity);
            return true;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Book Service", e);
        }
    }
}