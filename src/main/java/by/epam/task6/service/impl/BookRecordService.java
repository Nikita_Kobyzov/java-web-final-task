package by.epam.task6.service.impl;

import by.epam.task6.dao.BookDao;
import by.epam.task6.dao.BookRecordDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.DaoFactory;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.to.BookRecordTO;
import by.epam.task6.domain.vo.BookRecordVO;
import by.epam.task6.domain.vo.BackList;
import by.epam.task6.domain.vo.Page;
import by.epam.task6.service.*;
import by.epam.task6.service.Readable;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServerServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class is designed to solve problems of business logic for
 * a BookRecord-entity</p>
 * @author Nikita Kobyzov
 */
public class BookRecordService implements Readable<Integer, BookRecordVO>,
        Writable<BookRecordTO, BookRecordVO>,Updating<Integer, BookRecordTO> {
    private static final BookRecordService INSTANCE = new BookRecordService();
    private static final Validator validator = Validator.getInstance();

    /**
     * <p>This field specifies the number of books displayed on a page</p>
     */
    private static final int BOOKS_PER_PAGE = 4;

    private BookRecordService() {
    }

    public static BookRecordService getInstance() {
        return INSTANCE;
    }

    @Override
    public BookRecordVO write(BookRecordTO entity) throws ServiceException {
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
        BookDao bookDao = daoFactory.getBookDao();
        try {
            BookRecordTO bookRecordTO = bookRecordDao.create(entity);
            Book book = bookDao.read(entity.getId_book());
            BookRecordVO bookRecordVO = new BookRecordVO();
            bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
            bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
            bookRecordVO.setBook(book);
            bookRecordVO.setId(bookRecordTO.getId());
            return bookRecordVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    @Override
    public List<BookRecordVO> readAll() throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        BookDao bookDao = daoFactory.getBookDao();
        BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
        try {
            List<BookRecordTO> bookRecordTOList = bookRecordDao.read();
            List<BookRecordVO> bookRecordVOList = new ArrayList<BookRecordVO>();
            for (BookRecordTO bookRecordTO : bookRecordTOList) {
                BookRecordVO bookRecordVO = new BookRecordVO();
                bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
                bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
                bookRecordVO.setBook(bookDao.read(bookRecordTO.getId_book()));
                bookRecordVO.setId(bookRecordTO.getId());
                bookRecordVOList.add(bookRecordVO);
            }
            return bookRecordVOList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    @Override
    public boolean update(Integer key, BookRecordTO entity) throws ServiceException {
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            bookRecordDao.update(key, entity);
            return true;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    @Override
    public BookRecordVO read(Integer key) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        BookDao bookDao = daoFactory.getBookDao();
        try {
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            BookRecordTO bookRecordTO = bookRecordDao.read(key);
            if (bookRecordTO == null) {
                return null;
            }
            BookRecordVO bookRecordVO = new BookRecordVO();
            bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
            bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
            bookRecordVO.setId(bookRecordTO.getId());
            bookRecordVO.setBook(bookDao.read(bookRecordTO.getId_book()));
            return bookRecordVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    /**
     * <p>This method reads the book record on a book unique identifier</p>
     *
     * @param id is a unique identifier of the book
     * @return the object corresponding to the given book identifier or
     * <tt>null</tt> if there isn't entity with the appropriate key
     * @throws ServiceException if an error has occurred in the business logic
     */
    public BookRecordVO readByBookId(int id) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            BookDao bookDao = daoFactory.getBookDao();
            BookRecordTO bookRecordTO = bookRecordDao.readByBookId(id);
            if (bookRecordTO == null) {
                return null;
            }
            BookRecordVO bookRecordVO = new BookRecordVO();
            bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
            bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
            bookRecordVO.setId(bookRecordTO.getId());
            bookRecordVO.setBook(bookDao.read(bookRecordTO.getId_book()));
            return bookRecordVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    /**
     * <p>This method reads all available books for a user</p>
     *
     * @return a catalog of the available books for a user
     * @throws ServiceException if an error has occurred in the business logic
     */
    public BackList getBackList() throws ServiceException {
        BackList backList = new BackList();
        List<BookRecordVO> bookRecordVOList = this.readAll();
        List<Page> pages = new ArrayList<Page>();
        int element = 0;
        int countOfPages = 0;
        List<BookRecordVO> bookRecordVOs = new ArrayList<BookRecordVO>();
        if (bookRecordVOList.isEmpty()) {
            return backList;
        }
        BookRecordVO lastElement = bookRecordVOList.get(bookRecordVOList.size() - 1);
        for (BookRecordVO bookRecordVO : bookRecordVOList) {
            if (bookRecordVO.getFreeCount() != 0) {
                element++;
                bookRecordVOs.add(bookRecordVO);
            }
            if (element == BOOKS_PER_PAGE || bookRecordVO.equals(lastElement)) {
                countOfPages++;
                Page page = new Page();
                page.setId(countOfPages);
                page.setBookRecordVOList(bookRecordVOs);
                bookRecordVOs = new ArrayList<BookRecordVO>();
                pages.add(page);
                element = 0;
            }
        }
        backList.setPageList(pages);
        return backList;
    }

    /**
     * <p>This method reads all available books corresponding to the given genre
     * for a user</p>
     *
     * @param genre is a genre of the books
     * @return a catalog of the available books genre for a user
     * @throws ServiceException if an error has occurred in the business logic
     */
    public BackList getBackListOnGenre(String genre) throws ServiceException {
        BackList backList = new BackList();
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            List<Book> bookList = bookDao.readOnGenre(genre);
            List<BookRecordVO> bookRecordVOList = new ArrayList<BookRecordVO>();
            for (Book book : bookList) {
                BookRecordVO bookRecordVO = new BookRecordVO();
                BookRecordTO bookRecordTO = bookRecordDao.readByBookId(book.getId());
                bookRecordVO.setBook(book);
                bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
                bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
                bookRecordVO.setId(bookRecordTO.getId());
                bookRecordVOList.add(bookRecordVO);
            }
            if (bookRecordVOList.isEmpty()) {
                return backList;
            }
            List<Page> pages = new ArrayList<Page>();
            int element = 0;
            int countOfPages = 0;
            List<BookRecordVO> bookRecordVOs = new ArrayList<BookRecordVO>();
            BookRecordVO lastElement = bookRecordVOList.get(bookRecordVOList.size() - 1);
            for (BookRecordVO bookRecordVO : bookRecordVOList) {
                if (bookRecordVO.getFreeCount() != 0) {
                    element++;
                    bookRecordVOs.add(bookRecordVO);
                }
                if (element == BOOKS_PER_PAGE || bookRecordVO.equals(lastElement)) {
                    countOfPages++;
                    Page page = new Page();
                    page.setId(countOfPages);
                    page.setBookRecordVOList(bookRecordVOs);
                    bookRecordVOs = new ArrayList<BookRecordVO>();
                    pages.add(page);
                    element = 0;
                }
            }
            backList.setPageList(pages);
            return backList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

    /**
     * <p>This method reads all available books for an admin</p>
     *
     * @return a catalog of the available books for an admin
     * @throws ServiceException if an error has occurred in the business logic
     */
    public BackList getBackListForAdmin() throws ServiceException {
        BackList backList = new BackList();
        List<BookRecordVO> bookRecordVOList = this.readAll();
        if (bookRecordVOList.isEmpty()) {
            return backList;
        }
        List<Page> pages = new ArrayList<Page>();
        int element = 0;
        int countOfPages = 0;
        List<BookRecordVO> bookRecordVOs = new ArrayList<BookRecordVO>();
        BookRecordVO lastElement = bookRecordVOList.get(bookRecordVOList.size() - 1);
        for (BookRecordVO bookRecordVO : bookRecordVOList) {
            element++;
            bookRecordVOs.add(bookRecordVO);
            if (element == BOOKS_PER_PAGE || bookRecordVO.equals(lastElement)) {
                countOfPages++;
                Page page = new Page();
                page.setId(countOfPages);
                page.setBookRecordVOList(bookRecordVOs);
                bookRecordVOs = new ArrayList<BookRecordVO>();
                pages.add(page);
                element = 0;
            }
        }
        backList.setPageList(pages);
        return backList;
    }

    /**
     * <p>This method reads all available books corresponding to the given genre
     * for an admin</p>
     *
     * @param genre is a genre of the books
     * @return a catalog of the available books given genre for an admin
     * @throws ServiceException if an error has occurred in the business logic
     */
    public BackList getBackListOnGenreForAdmin(String genre) throws ServiceException {
        BackList backList = new BackList();
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            BookDao bookDao = daoFactory.getBookDao();
            BookRecordDao bookRecordDao = daoFactory.getBookRecordDao();
            List<Book> bookList = bookDao.readOnGenre(genre);
            List<BookRecordVO> bookRecordVOList = new ArrayList<BookRecordVO>();
            for (Book book : bookList) {
                BookRecordVO bookRecordVO = new BookRecordVO();
                BookRecordTO bookRecordTO = bookRecordDao.readByBookId(book.getId());
                bookRecordVO.setBook(book);
                bookRecordVO.setFreeCount(bookRecordTO.getFreeCount());
                bookRecordVO.setTotalCount(bookRecordTO.getTotalCount());
                bookRecordVO.setId(bookRecordTO.getId());
                bookRecordVOList.add(bookRecordVO);
            }
            if (bookRecordVOList.isEmpty()) {
                return backList;
            }
            List<Page> pages = new ArrayList<Page>();
            int element = 0;
            int countOfPages = 0;
            List<BookRecordVO> bookRecordVOs = new ArrayList<BookRecordVO>();
            BookRecordVO lastElement = bookRecordVOList.get(bookRecordVOList.size() - 1);
            for (BookRecordVO bookRecordVO : bookRecordVOList) {
                element++;
                bookRecordVOs.add(bookRecordVO);
                if (element == BOOKS_PER_PAGE || bookRecordVO.equals(lastElement)) {
                    countOfPages++;
                    Page page = new Page();
                    page.setId(countOfPages);
                    page.setBookRecordVOList(bookRecordVOs);
                    bookRecordVOs = new ArrayList<BookRecordVO>();
                    pages.add(page);
                    element = 0;
                }
            }
            backList.setPageList(pages);
            return backList;

        } catch (DaoException e) {
            throw new ServerServiceException("Exception in BookRecord Service", e);
        }
    }

}