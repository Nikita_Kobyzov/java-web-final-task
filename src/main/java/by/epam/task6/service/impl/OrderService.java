package by.epam.task6.service.impl;

import by.epam.task6.dao.*;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.domain.Book;
import by.epam.task6.domain.User;
import by.epam.task6.domain.to.OrderTO;
import by.epam.task6.domain.vo.OrderVO;
import by.epam.task6.service.Deleting;
import by.epam.task6.service.Updating;
import by.epam.task6.service.Writable;
import by.epam.task6.service.exception.ClientServiceException;
import by.epam.task6.service.exception.ServerServiceException;
import by.epam.task6.service.exception.ServiceException;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import by.epam.task6.service.Readable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class is designed to solve problems of business logic for
 * a Order-entity</p>
 * @author Nikita Kobyzov
 */
public class OrderService implements Writable<OrderTO, OrderVO>, Readable<Integer, OrderVO>,
        Updating<Integer, OrderTO>, Deleting<Integer> {
    private static final OrderService INSTANCE = new OrderService();
    private static final Validator validator = Validator.getInstance();

    private OrderService() {
    }

    public static OrderService getInstance() {
        return INSTANCE;
    }

    @Override
    public OrderVO write(OrderTO entity) throws ServiceException {
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        OrderDao orderDao = daoFactory.getOrderDao();
        BookDao bookDao = daoFactory.getBookDao();
        try {
            OrderTO orderTO = orderDao.create(entity);
            OrderVO orderVO = new OrderVO();
            orderVO.setId(orderTO.getId());
            Book book = bookDao.read(entity.getId_book());
            orderVO.setBook(book);
            UserDao userDao = daoFactory.getUserDao();
            User user = userDao.read(entity.getId_client());
            orderVO.setUser(user);
            return orderVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    @Override
    public List<OrderVO> readAll() throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            BookDao bookDao = daoFactory.getBookDao();
            UserDao userDao = daoFactory.getUserDao();
            List<OrderTO> orderTOList = orderDao.read();
            List<OrderVO> orderVOList = new ArrayList<OrderVO>();
            for (OrderTO orderTO : orderTOList) {
                OrderVO orderVO = new OrderVO();
                orderVO.setId(orderTO.getId());
                orderVO.setConfirmation(orderTO.isConfirmation());
                orderVO.setReadingPlace(orderTO.getReadingPlace());
                orderVO.setBook(bookDao.read(orderTO.getId_book()));
                orderVO.setUser(userDao.read(orderTO.getId_client()));
                orderVOList.add(orderVO);
            }
            return orderVOList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    /**
     * <p>This method reads all confirmed orders</p>
     *
     * @return a list of all confirmed orders
     * @throws ServiceException if an error has occurred in the business logic
     */
    public List<OrderVO> readAllConfirmed() throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            BookDao bookDao = daoFactory.getBookDao();
            UserDao userDao = daoFactory.getUserDao();
            List<OrderTO> orderTOList = orderDao.read();
            List<OrderVO> orderVOList = new ArrayList<OrderVO>();
            for (OrderTO orderTO : orderTOList) {
                if (!orderTO.isConfirmation()) {
                    continue;
                }
                OrderVO orderVO = new OrderVO();
                orderVO.setId(orderTO.getId());
                orderVO.setConfirmation(orderTO.isConfirmation());
                orderVO.setReadingPlace(orderTO.getReadingPlace());
                orderVO.setBook(bookDao.read(orderTO.getId_book()));
                orderVO.setUser(userDao.read(orderTO.getId_client()));
                orderVOList.add(orderVO);
            }
            return orderVOList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    /**
     * <p>This method reads all not confirmed orders</p>
     *
     * @return a list of all not confirmed orders
     * @throws ServiceException if an error has occurred in the business logic
     */
    public List<OrderVO> readAllNotConfirmed() throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            BookDao bookDao = daoFactory.getBookDao();
            UserDao userDao = daoFactory.getUserDao();
            List<OrderTO> orderTOList = orderDao.read();
            List<OrderVO> orderVOList = new ArrayList<OrderVO>();
            for (OrderTO orderTO : orderTOList) {
                if (orderTO.isConfirmation()) {
                    continue;
                }
                OrderVO orderVO = new OrderVO();
                orderVO.setId(orderTO.getId());
                orderVO.setConfirmation(orderTO.isConfirmation());
                orderVO.setReadingPlace(orderTO.getReadingPlace());
                orderVO.setBook(bookDao.read(orderTO.getId_book()));
                orderVO.setUser(userDao.read(orderTO.getId_client()));
                orderVOList.add(orderVO);
            }
            return orderVOList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    /**
     * <p>This method reads the order records on a user unique identifier</p>
     *
     * @param id is a unique identifier of the user
     * @return a list of orders corresponding to the given user id
     * @throws ServiceException if an error has occurred in the business logic
     */
    public List<OrderVO> readAllByUserId(int id) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            BookDao bookDao = daoFactory.getBookDao();
            UserDao userDao = daoFactory.getUserDao();
            List<OrderTO> orderTOList = orderDao.readByUserId(id);
            List<OrderVO> orderVOList = new ArrayList<OrderVO>();
            for (OrderTO orderTO : orderTOList) {
                OrderVO orderVO = new OrderVO();
                orderVO.setId(orderTO.getId());
                orderVO.setConfirmation(orderTO.isConfirmation());
                orderVO.setReadingPlace(orderTO.getReadingPlace());
                orderVO.setBook(bookDao.read(orderTO.getId_book()));
                orderVO.setUser(userDao.read(orderTO.getId_client()));
                orderVOList.add(orderVO);
            }
            return orderVOList;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    @Override
    public OrderVO read(Integer key) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            BookDao bookDao = daoFactory.getBookDao();
            UserDao userDao = daoFactory.getUserDao();
            OrderTO orderTO = orderDao.read(key);
            OrderVO orderVO = new OrderVO();
            orderVO.setId(orderTO.getId());
            orderVO.setConfirmation(orderTO.isConfirmation());
            orderVO.setReadingPlace(orderTO.getReadingPlace());
            orderVO.setBook(bookDao.read(orderTO.getId_book()));
            orderVO.setUser(userDao.read(orderTO.getId_client()));
            return orderVO;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    @Override
    public boolean update(Integer key, OrderTO entity) throws ServiceException {
        try {
            validator.validate(entity);
        } catch (ValidatorException e) {
            throw new ClientServiceException("Validation exception", e);
        }
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            orderDao.update(key, entity);
            return true;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }

    @Override
    public boolean delete(Integer key) throws ServiceException {
        try {
            DaoFactory daoFactory = DaoFactory.getDaoFactory();
            OrderDao orderDao = daoFactory.getOrderDao();
            orderDao.delete(key);
            return true;
        } catch (DaoException e) {
            throw new ServerServiceException("Exception in Order Service", e);
        }
    }
}