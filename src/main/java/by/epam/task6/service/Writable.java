package by.epam.task6.service;

import by.epam.task6.service.exception.ServiceException;

/**
 * <p>This interface describes that classes which implements this
 * interface have the ability to perform the record operation</p>
 * @param <TO> describes the type of the entity
 * @param <TO> describes the type of the recorded entity
 * @author Nikita Kobyzov
 */
public interface Writable<TO, VO> {
    /**
     * <p>This method writes a new object</p>
     *
     * @param entity is the object that to be recorded
     * @return the object that was recorded
     * @throws ServiceException if an error has occurred with the write operations
     */
    VO write(TO entity) throws ServiceException;
}
