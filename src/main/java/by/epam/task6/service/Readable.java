package by.epam.task6.service;

import by.epam.task6.service.exception.ServiceException;

import java.util.List;

/**
 * <p>This interface describes that classes which implements this
 * interface have the ability to perform the search operation</p>
 * @param <K> describes a unique identifier of the entity
 * @param <VO> describes the type of the entity
 * @author Nikita Kobyzov
 */
public interface Readable<K, VO> {
    /**
     * <p>This method reads all object</p>
     *
     * @return a list of all found objects
     * @throws ServiceException if an error has occurred with the search operations
     */
    List<VO> readAll() throws ServiceException;

    /**
     * <p>This method reads the object on a key</p>
     *
     * @param key is a unique identifier of the entity
     * @return the object corresponding to the given key or <tt>null</tt> if
     * there isn't entity with the appropriate key
     * @throws ServiceException if an error has occurred with the search operations
     */
    VO read(K key) throws ServiceException;
}