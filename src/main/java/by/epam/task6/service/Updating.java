package by.epam.task6.service;

import by.epam.task6.service.exception.ServiceException;

/**
 * <p>This interface describes that classes which implements this
 * interface have the ability to perform the update operation</p>
 * @param <K> describes a unique identifier of the entity
 * @param <TO> describes the type of the entity
 * @author Nikita Kobyzov
 */
public interface Updating<K, TO> {
    /**
     * <p>This method delete an object on a key</p>
     *
     * @param key    is a unique identifier of the entity that to be changed
     * @param entity is a object that to be recorded
     * @return <tt>true</tt> is the object was updated
     * @throws ServiceException if an error has occurred with the update operations
     */
    boolean update(K key, TO entity) throws ServiceException;
}
