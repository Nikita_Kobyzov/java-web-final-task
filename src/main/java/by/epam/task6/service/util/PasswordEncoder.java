package by.epam.task6.service.util;

/**
 * <p>This class is used for encoding and decoding the user's password. This class
 * uses an algorithm XOR-encryption.</p>
 * @author Nikita Kobyzov
 */
public final class PasswordEncoder {
    /**
     * <p>This field is used as a key for encoding/p>
     */
    private static final String KEY_STRING="I_LOVE_JAVA";

    /**
     * <p>This method encodes the input string</p>
     * @param password is a user's password
     * @return encoded string
     */
    public static String encode(String password){
        byte[] key=KEY_STRING.getBytes();
        byte[] passwordBytes=password.getBytes();
        byte[] result=new byte[password.length()];
        for (int i = 0; i < passwordBytes.length; i++) {
            result[i] = (byte) (passwordBytes[i] ^ key[i % key.length]);
        }
        return new String(result);
    }

    /**
     * <p>This method encodes the input string</p>
     * @param password  is a user's encoding password
     * @return decoded string
     */
    public static String decode(String password){
        byte[] key = KEY_STRING.getBytes();
        byte[] code=password.getBytes();
        byte[] result = new byte[code.length];
        for (int i = 0; i < code.length; i++) {
            result[i] = (byte) (code[i] ^ key[i % key.length]);
        }
        return new String(result);
    }
}
