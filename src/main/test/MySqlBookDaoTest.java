import by.epam.task6.dao.BookDao;
import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.dao.sql.MySqlBookDao;
import by.epam.task6.domain.Book;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by NikitOS on 22.11.2015.
 */
public class MySqlBookDaoTest {
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String url = "jdbc:mysql://localhost/library";
    private static final String user = "root";
    private static final String password = "nikita5849";
    private static final int size = 5;

    private static final Book testBook = new Book();
    private ConnectionPool connectionPool;

    @Before
    public void initCP() throws ConnectionPoolException, DaoException {
        connectionPool = ConnectionPool.getInstance();
        connectionPool.init(url, driver, user, password, size);
        testBook.setTitle("test");
        testBook.setSize(2);
        testBook.setAuthor("test");
        testBook.setGenre("test");
        testBook.setImage("test");
        testBook.setAnnotation("test");
        BookDao bookDao = MySqlBookDao.getInstance();
        Book dbBook = bookDao.create(testBook);
        testBook.setId(dbBook.getId());
    }

    @After
    public void destroyCP() throws ConnectionPoolException, DaoException {
        BookDao bookDao = MySqlBookDao.getInstance();
        bookDao.delete(testBook.getId());
        ConnectionPool.getInstance().destroy();
    }


    @Test
    public void testBook() throws DaoException {
        BookDao bookDao = MySqlBookDao.getInstance();
        Assert.assertEquals(testBook, bookDao.read(testBook.getId()));
    }

    @Test
    public void updateTestBookDao() throws DaoException {
        BookDao bookDao = MySqlBookDao.getInstance();
        Book book = new Book();
        book.setTitle("upTest");
        book.setImage("upImage");
        book.setGenre("upGenre");
        book.setAuthor("upAuthor");
        book.setAnnotation("upAnnotation");
        book.setId(testBook.getId());
        book.setSize(23);
        Book dbBook = bookDao.update(testBook.getId(), book);
        Assert.assertEquals(testBook, dbBook);
        Assert.assertEquals(book, bookDao.read(testBook.getId()));
    }
}
