import by.epam.task6.service.util.PasswordEncoder;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by NikitOS on 22.12.2015.
 */
public class PasswordEncoderTest {
    private static final String TEST_STRING = "This is test string";
    private static final PasswordEncoder PASSWORD_ENCODER = new PasswordEncoder();

    @Test
    public void test() {
        String encodeString = PASSWORD_ENCODER.encode(TEST_STRING);
        Assert.assertFalse(TEST_STRING.equals(encodeString));
        String decodeString = PASSWORD_ENCODER.decode(encodeString);
        Assert.assertEquals(TEST_STRING, decodeString);
    }
}
