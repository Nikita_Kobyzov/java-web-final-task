import by.epam.task6.dao.exception.DaoException;
import by.epam.task6.dao.UserDao;
import by.epam.task6.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.task6.dao.connectionpool.impl.ConnectionPool;
import by.epam.task6.dao.sql.MySqlUserDao;
import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by NikitOS on 16.11.2015.
 */
public class MySqlUserDaoTest {
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String url = "jdbc:mysql://localhost/library";
    private static final String user = "root";
    private static final String password = "nikita5849";
    private static final int size = 5;

    private static final User testUser = new User();
    private ConnectionPool connectionPool;

    @Before
    public void initCP() throws ConnectionPoolException, DaoException {
        connectionPool = ConnectionPool.getInstance();
        connectionPool.init(url, driver, user, password, size);
        testUser.setLogin("test0");
        testUser.setPassword("=:?;");
        testUser.setfName("test");
        testUser.setlName("test");
        testUser.setNumber(12345);
        testUser.setUserStatus(UserStatus.CLIENT);
        UserDao userDao = MySqlUserDao.getInstance();
        User dbUser = userDao.create(testUser);
        testUser.setId(dbUser.getId());
    }

    @After
    public void destroyCP() throws ConnectionPoolException, DaoException {
        UserDao userDao = MySqlUserDao.getInstance();
        userDao.delete(testUser.getId());
        ConnectionPool.getInstance().destroy();
    }

    @Test
    public void findTestUserDao() throws DaoException {
        UserDao userDao = MySqlUserDao.getInstance();
        Assert.assertEquals(testUser, userDao.read(testUser.getLogin()));
        Assert.assertEquals(testUser, userDao.read(testUser.getId()));

    }

    @Test
    public void updateTestUserDao() throws DaoException {
        UserDao userDao = MySqlUserDao.getInstance();
        User updatingUser = new User();
        updatingUser.setLogin("test1");
        updatingUser.setPassword("test1");
        updatingUser.setNumber(54321);
        updatingUser.setfName("test1");
        updatingUser.setlName("test1");
        updatingUser.setUserStatus(UserStatus.ADMIN);
        updatingUser.setId(testUser.getId());
        User dbUser = userDao.update(updatingUser.getId(), updatingUser);
        Assert.assertEquals(testUser, dbUser);
        Assert.assertEquals(updatingUser.getLogin(), userDao.read(testUser.getId()).getLogin());
    }
}
