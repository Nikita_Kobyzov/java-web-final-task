import by.epam.task6.domain.Book;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import junit.framework.Assert;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;


/**
 * Created by NikitOS on 19.11.2015.
 */
public class BookValidatorTest {
    private static final Logger LOG = LogManager.getLogger(BookValidatorTest.class);
    private static final Validator validator = Validator.getInstance();
    private static final Book wrongBook = new Book();
    private static final Book rightBook = new Book();

    @Before
    public void init() {
        rightBook.setAuthor("Автор");
        rightBook.setImage("img.jpg");
        rightBook.setGenre("Detective");
        rightBook.setSize(23);
        rightBook.setAnnotation("Аннотация");
        wrongBook.setAuthor("Автор");
        wrongBook.setImage("img .jpg");
        wrongBook.setGenre("Detective");
        wrongBook.setSize(23);
        wrongBook.setAnnotation("annotation");
    }

    @Test
    public void TestRight() {
        boolean result;
        try {
            result = validator.validate(rightBook);
        } catch (ValidatorException e) {
            LOG.warn("Object is not valid", e);
            result = false;
        }
        Assert.assertTrue(result);
    }

    @Test
    public void TestWrong() {
        boolean result;
        try {
            result = validator.validate(wrongBook);
        } catch (ValidatorException e) {
            LOG.info("Object is not valid", e);
            result = false;
        }
        Assert.assertFalse(result);
    }
}
