import by.epam.task6.domain.User;
import by.epam.task6.domain.UserStatus;
import by.epam.task6.validator.Validator;
import by.epam.task6.validator.exception.ValidatorException;
import junit.framework.Assert;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by NikitOS on 17.11.2015.
 */
public class UserValidatorTest {
    private static final Logger LOG = LogManager.getLogger(UserValidatorTest.class);
    private static final Validator validator = Validator.getInstance();
    private static final User wrongUser = new User();
    private static final User rightUser = new User();

    @Before
    public void init() {
        wrongUser.setLogin("bad");
        wrongUser.setPassword("bad");
        wrongUser.setfName("плохо");
        wrongUser.setlName("плохо");
        wrongUser.setNumber(123456789);
        wrongUser.setUserStatus(UserStatus.CLIENT);
        rightUser.setLogin("goodLogin");
        rightUser.setPassword("goodPassword");
        rightUser.setfName("Правильное имя");
        rightUser.setlName("Правильная фамилия");
        rightUser.setNumber(123456789);
        rightUser.setUserStatus(UserStatus.ADMIN);
    }

    @Test
    public void TestRight() {
        boolean result;
        try {
            result = validator.validate(rightUser);
        } catch (ValidatorException e) {
            LOG.warn("Object is not valid", e);
            result = false;
        }
        Assert.assertTrue(result);
    }

    @Test
    public void TestWring() {
        boolean result;
        try {
            result = validator.validate(wrongUser);
        } catch (ValidatorException e) {
            LOG.info("Object is not valid", e);
            result = false;
        }
        Assert.assertFalse(result);
    }
}
