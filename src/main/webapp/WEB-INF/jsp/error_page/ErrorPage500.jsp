<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<c:set var="status" scope="page" value="NONE"/>
<c:if test="${sessionScope.user!=null}" var="isLogin">
  <jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
  <c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
</c:if>
<html>
<head>
  <title>Digital Library</title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/button.css">
  <link rel="stylesheet" type="text/css" href="css/loginform.css">
  <link rel="stylesheet" type="text/css" href="css/btn3d.css">
  <script src="js/main.js" type="text/javascript"></script>
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale" title="Русский"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&PAGE=${pageContext.request.requestURI}">
              <img class="locale" src="image/ru.svg">
            </a></li>
            <li><a class="locale" title="English"
                   href="libraryController?ACTION=change_language&LANGUAGE=en&PAGE=${pageContext.request.requestURI}">
              <img class="locale" src="image/us.svg">
            </a></li>
            <c:if test="${status!='NONE'}">
              <li><a  onmousedown="return false">
                <span class="glyphicon glyphicon-user"></span> ${user.login}
              </a>
              </li>
              <li><a href="libraryController?ACTION=logout">
                <span class="glyphicon glyphicon-log-out"></span>
                <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
              </a>
              </li>
            </c:if>
            <c:if test="${status=='NONE'}">
              <li><a href="index.jsp">
                <span class="glyphicon glyphicon-log-in"></span>
                <fmt:message key="locale.form.button.login" bundle="${lang}"/>
              </a></li>
            </c:if>
          </ul>
          <ul class="nav navbar-nav">
            <c:if test="${status=='CLIENT'}">
              <li><a href="libraryController?ACTION=USER_CART">
                <fmt:message key="locale.link.cart" bundle="${lang}"/>
              </a>
              </li>
              <li><a href="libraryController?ACTION=ORDER_LIST">
                <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
              </a>
              </li>
            </c:if>
            <c:if test="${status=='ADMIN'}">
              <li><a href="libraryController?ACTION=ORDER_LIST">
                <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
              </a>
              </li>
            </c:if>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br><br>
<img src="/image/error_img/error500.png">
<hr>
<footer class="copyright">
  <i>
    <user-tag:copyright name="Nikita Kobyzov"/>
  </i>
</footer>

</body>
</html>
