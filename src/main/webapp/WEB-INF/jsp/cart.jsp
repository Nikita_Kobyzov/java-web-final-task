
<%--
  Created by IntelliJ IDEA.
  User: NikitOS
  Date: 11.12.2015
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
<c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
<jsp:useBean id="cartList"  type="java.util.List<by.epam.task6.domain.vo.CartVO>" scope="request"/>
<html>
<head>
  <title><fmt:message key="locale.title.cart" bundle="${lang}"/></title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/button.css">
  <link rel="stylesheet" type="text/css" href="css/btn3d.css">
  <link rel="stylesheet" type="text/css" href="css/loginform.css">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="js/main.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&LAST_COMMAND=USER_CART">
              <img class="locale" src="image/ru.svg">
            </a></li>
            <li><a class="locale" href="libraryController?ACTION=change_language&LANGUAGE=en&LAST_COMMAND=USER_CART">
              <img class="locale" src="image/us.svg">
            </a></li>
              <li><a  onmousedown="return false">
                <span class="glyphicon glyphicon-user"></span> ${user.login}
              </a></li>
              <li><a href="libraryController?ACTION=logout">
                <span class="glyphicon glyphicon-log-out"></span>
                <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
              </a></li>
          </ul>
          <ul class="nav navbar-nav">
              <li><a href="libraryController?ACTION=USER_CART">
                <fmt:message key="locale.link.cart" bundle="${lang}"/>
              </a></li>
              <li><a href="libraryController?ACTION=ORDER_LIST">
                <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
              </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
      <div class="list-group">
        <c:if test="${cartList.size()==0}">
          <br><br><br>
              <div class="alert alert-info">
                <h1><fmt:message key="locale.message.empty_cart" bundle="${lang}"/></h1>
              </div>
        </c:if>
        <c:forEach items="${cartList}" var="cart">
          <div class="list-group-item">
            <div class="row">
              <div class="col-xs-6 col-md-3">
                <a href="libraryController?ACTION=BOOK_VIEW&ID=${cart.book.id}" class="thumbnail">
                  <img src="image/book/${cart.book.image}" alt="book">
                </a>
              </div>
              <div class="col-xs-6 col-md-8">
                <h2><a href="libraryController?ACTION=BOOK_VIEW&ID=${cart.book.id}" >${cart.book.title}</a></h2>
                <form action="libraryController" method="post">
                  <input type="hidden" name="ACTION" value="TAKE_BOOK">
                  <input type="hidden" name="CART_ID" value="${cart.id}">
                  <input type="hidden" name="BOOK_ID" value="${cart.book.id}">
                  <label class="radio-inline">
                    <input type="radio" value="at_hall" name="READING_PLACE">
                    <fmt:message key="locale.form.label.hall" bundle="${lang}"/>
                  </label>
                  <label class="radio-inline">
                    <input checked type="radio" value="on_subscription" name="READING_PLACE">
                    <fmt:message key="locale.form.label.subscription" bundle="${lang}"/>
                  </label>
                  <hr>
                  <button type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-ok"></span>
                  </button>
                  <button form="remove"  type="submit" class="btn btn-danger">
                    <span class="glyphicon glyphicon-remove"></span>
                  </button>
                </form>
                <form id="remove" action="libraryController" method="post">
                  <input type="hidden" name="ACTION" value="REMOVE_BOOK_FROM_CART">
                  <input type="hidden" name="CART_ID" value="${cart.id}">
                  <input type="hidden" name="BOOK_ID" value="${cart.book.id}">
                </form>
              </div>
            </div>
          </div>
        </c:forEach>
      </div>
    </div>
  </div>
</div>
<hr>
<footer class="copyright">
  <i>
    <user-tag:copyright name="Nikita Kobyzov"/>
  </i>
</footer>
</body>
</html>
