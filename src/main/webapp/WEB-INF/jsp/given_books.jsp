<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
<c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
<jsp:useBean id="confirmOrders"  type="java.util.List<by.epam.task6.domain.vo.OrderVO>" scope="request"/>
<jsp:useBean id="noConfirmOrders"  type="java.util.List<by.epam.task6.domain.vo.OrderVO>" scope="request"/>
<html>
<head>
  <title><fmt:message key="locale.title.order_list" bundle="${lang}"/></title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/button.css">
  <link rel="stylesheet" type="text/css" href="css/btn3d.css">
  <link rel="stylesheet" type="text/css" href="css/loginform.css">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.min.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css.map" rel="stylesheet">
  <script src="bootstrap-3.3.6-dist/js/bootstrap.js"></script>
  <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  <script src="bootstrap-3.3.6-dist/js/npm.js"></script>
  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/jquery.js"></script>
  <script src="js/jq-1.1.3.js" type="text/javascript"></script>
  <script src="js/jq-2.1.4.js" type="text/javascript"></script>
  <script src="js/jq-min.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&LAST_COMMAND=ADMIN_ORDER_LIST">
              <img class="locale" src="image/ru.svg">
            </a></li>
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=en&LAST_COMMAND=ADMIN_ORDER_LIST">
              <img class="locale" src="image/us.svg">
            </a></li>
            <li><a  onmousedown="return false">
              <span class="glyphicon glyphicon-user"></span> ${user.login}
            </a></li>
            <li><a href="libraryController?ACTION=logout">
              <span class="glyphicon glyphicon-log-out"></span>
              <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
            </a></li>
          </ul>
          <ul class="nav navbar-nav">
            <li><a href="libraryController?ACTION=ADMIN_ORDER_LIST">
              <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
            </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6"><div class="list-group">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <br>
            <div class="tabs">
              <ul class="nav nav-tabs">

                <li class="active confirm"><a class="btn btn-lg" href="#tab-2" data-toggle="tab">
                  <fmt:message key="locale.info.not_confirmed" bundle="${lang}"/>
                  <span class="badge">${noConfirmOrders.size()}</span>
                </a></li>
                <li class="confirm"><a class="btn btn-lg" href="#tab-1" data-toggle="tab">
                  <fmt:message key="locale.info.confirmed" bundle="${lang}"/>
                  <span class="badge">${confirmOrders.size()}</span>
                </a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade" id="tab-1">
                  <c:if test="${confirmOrders.size()==0}">
                    <br><br><br>
                    <div class="alert alert-info">
                      <h1><fmt:message key="locale.message.empty_order" bundle="${lang}"/></h1>
                    </div>
                  </c:if>
                  <c:forEach items="${confirmOrders}" var="order">
                    <div class="list-group-item">
                      <div class="row">
                        <div class="col-xs-6 col-md-3">
                          <a href="libraryController?ACTION=BOOK_VIEW&ID=${order.book.id}" class="thumbnail">
                            <img src="image/book/${order.book.image}" alt="book">
                          </a>
                        </div>
                        <div class="col-xs-6 col-md-8">
                          <h2><a href="libraryController?ACTION=BOOK_VIEW&ID=${order.book.id}" >${order.book.title}</a></h2>
                          <ul class="text-left">
                            <li>
                              <strong><fmt:message key="locale.info.order_state" bundle="${lang}"/>:</strong>
                              <fmt:message key="locale.info.confirmation_${order.confirmation}" bundle="${lang}"/>
                            </li>
                            <li>
                              <strong><fmt:message key="locale.info.reading_place" bundle="${lang}"/>:</strong>
                              <fmt:message key="locale.info.${order.readingPlace}" bundle="${lang}"/>
                            </li>
                            <li><strong><fmt:message key="locale.info.user.user" bundle="${lang}"/>:</strong>
                              <a class="btn-link button-link" onclick="setBookID(${book_record.book.id})"
                                 data-toggle="modal" data-target="#modal-${order.id}">${order.user.login}</a>
                            </li>
                          </ul>
                          <form id="${order.id}" action="libraryController" method="post">
                            <input type="hidden" name="ID" value="${order.id}">
                            <input form="${order.id}" type="hidden" name="ACTION" value="DELETE_ORDER">
                            <input form="${order.id}" type="submit" class="btn btn-primary"
                                   value="<fmt:message key="locale.form.button.delete" bundle="${lang}"/>">
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade"  id="modal-${order.id}">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">
                              <i class="glyphicon glyphicon-remove"></i>
                            </button>
                            <h4 class="modal-title"><fmt:message key="locale.info.user.user" bundle="${lang}"/></h4>
                          </div>
                          <div class="modal-body">
                            <ul class="list-group">
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.info.user.login" bundle="${lang}"/>:</strong>
                                  ${order.user.login}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.fName" bundle="${lang}"/>:</strong>
                                  ${order.user.fName}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.lName" bundle="${lang}"/>:</strong>
                                  ${order.user.lName}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.phoneNumber" bundle="${lang}"/>:</strong>
                                  ${order.user.number}
                              </li>
                            </ul>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn3d btn-danger" type="button" data-dismiss="modal">
                              <fmt:message key="locale.form.button.cancel" bundle="${lang}"/>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </c:forEach>
                </div>
                <div class="tab-pane fade in active" id="tab-2">
                  <c:if test="${noConfirmOrders.size()==0}">
                    <br><br><br>
                    <div class="alert alert-info">
                      <h1><fmt:message key="locale.message.empty_order" bundle="${lang}"/></h1>
                    </div>
                  </c:if>
                  <c:forEach items="${noConfirmOrders}" var="order">
                    <div class="list-group-item">
                      <div class="row">
                        <div class="col-xs-6 col-md-3">
                          <a href="libraryController?ACTION=BOOK_VIEW&ID=${order.book.id}" class="thumbnail">
                            <img src="image/book/${order.book.image}" alt="book">
                          </a>
                        </div>
                        <div class="col-xs-6 col-md-8">
                          <h2><a href="libraryController?ACTION=BOOK_VIEW&ID=${order.book.id}" >${order.book.title}</a></h2>
                          <ul class="text-left">
                            <li>
                              <strong><fmt:message key="locale.info.order_state" bundle="${lang}"/>:</strong>
                              <fmt:message key="locale.info.confirmation_${order.confirmation}" bundle="${lang}"/>
                            </li>
                            <li>
                              <strong><fmt:message key="locale.info.reading_place" bundle="${lang}"/>:</strong>
                              <fmt:message key="locale.info.${order.readingPlace}" bundle="${lang}"/>
                            </li>
                            <li><strong><fmt:message key="locale.info.user.user" bundle="${lang}"/>:</strong>
                              <a class="btn-link button-link" onclick="setBookID(${book_record.book.id})"
                                 data-toggle="modal" data-target="#modal-${order.id}">${order.user.login}</a>
                            </li>
                          </ul>
                          <form id="${order.id}" action="libraryController" method="post">
                            <input type="hidden" name="ID" value="${order.id}">
                            <input form="${order.id}" type="hidden" name="ACTION" value="CONFIRM_ORDER">
                            <input form="${order.id}" type="submit" class="btn btn-success"
                                   value="<fmt:message key="locale.form.button.confirm" bundle="${lang}"/>"/>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade"  id="modal-${order.id}">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">
                              <i class="glyphicon glyphicon-remove"></i>
                            </button>
                            <h4 class="modal-title"><fmt:message key="locale.info.user.user" bundle="${lang}"/></h4>
                          </div>
                          <div class="modal-body">
                            <ul class="list-group">
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.info.user.login" bundle="${lang}"/>:</strong>
                                  ${order.user.login}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.fName" bundle="${lang}"/>:</strong>
                                  ${order.user.fName}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.lName" bundle="${lang}"/>:</strong>
                                  ${order.user.lName}
                              </li>
                              <li class="list-group-item">
                                <strong><fmt:message key="locale.form.label.phoneNumber" bundle="${lang}"/>:</strong>
                                  ${order.user.number}
                              </li>
                            </ul>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn3d btn-danger" type="button" data-dismiss="modal">
                              <fmt:message key="locale.form.button.cancel" bundle="${lang}"/>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </c:forEach>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div></div>
  </div>
</div>
<hr>
<footer class="copyright">
  <i>
    <user-tag:copyright name="Nikita Kobyzov"/>
  </i>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jq-min.js" type="text/javascript"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>
