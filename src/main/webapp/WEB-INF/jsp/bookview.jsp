<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<c:set var="status" scope="page" value="NONE"/>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<c:if test="${sessionScope.user!=null}" var="isLogin">
  <jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
  <c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
</c:if>
<jsp:useBean id="book" class="by.epam.task6.domain.Book" scope="request"/>
<html>
<head>
  <title><fmt:message key="locale.title.book_view" bundle="${lang}"/></title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/button.css">
  <link rel="stylesheet" type="text/css" href="css/btn3d.css">
  <link rel="stylesheet" type="text/css" href="css/loginform.css">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&LAST_COMMAND=book_view&ID=${book.id}">
              <img class="locale" src="image/ru.svg">
            </a></li>
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=en&LAST_COMMAND=book_view&ID=${book.id}">
              <img class="locale" src="image/us.svg">
            </a></li>
            <c:if test="${status!='NONE'}">
              <li><a  onmousedown="return false">
                <span class="glyphicon glyphicon-user"></span>
                ${user.login}
              </a></li>
              <li><a href="libraryController?ACTION=logout">
                <span class="glyphicon glyphicon-log-out"></span>
                <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
              </a></li>
            </c:if>
            <c:if test="${status=='NONE'}">
              <li><a href="index.jsp"><span class="glyphicon glyphicon-log-in"></span>
                <fmt:message key="locale.form.button.login" bundle="${lang}"/>
              </a></li>
            </c:if>
          </ul>
          <ul class="nav navbar-nav">
            <c:if test="${status=='CLIENT'}">
              <li><a href="libraryController?ACTION=USER_CART">
                <fmt:message key="locale.link.cart" bundle="${lang}"/>
              </a></li>
              <li><a href="libraryController?ACTION=ORDER_LIST">
                <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
              </a></li>
            </c:if>
            <c:if test="${status=='ADMIN'}">
              <li><a href="libraryController?ACTION=ADMIN_ORDER_LIST">
                <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
              </a></li>
            </c:if>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br><br>
<div class="container">
  <div class="row">
    <div class="col-lg-4">
      <div class="img-thumbnail">
        <img src="image/book/${book.image}" width="300px">
      </div>
    </div>
    <div class="col-lg-6 text-left lead">
      <h1>${book.title}</h1>
      <c:if test="${user.userStatus=='CLIENT'}">
        <form method="post" action="libraryController">
          <input type="hidden" name="ACTION" value="TO_CART">
          <input type="hidden" name="BOOK_ID" value="${book.id}">
          <button type="submit" class="brn btn-block btn-success">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <fmt:message key="locale.link.to_cart" bundle="${lang}"/>
          </button>
        </form>
      </c:if>
      <ul>
        <li>
          <strong><fmt:message key="locale.info.book.author" bundle="${lang}"/>:</strong>
          ${book.author}
        </li>
        <li>
          <strong><fmt:message key="locale.info.book.size" bundle="${lang}"/>:</strong>
          ${book.size}
          <fmt:message key="locale.info.pages" bundle="${lang}"/>
        </li>
        <li>
          <strong><fmt:message key="locale.info.book.genre" bundle="${lang}"/>:</strong>
          <fmt:message key="locale.info.genre.${book.genre}" bundle="${lang}"/>
        </li>
      </ul>
      <p>${book.annotation}</p>
    </div>
  </div>
</div>
<hr>
<footer class="copyright">
  <i>
    <user-tag:copyright name="Nikita Kobyzov"/>
  </i>
</footer>

</body>
</html>
