<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<c:set var="status" scope="page" value="NONE"/>
<c:if test="${sessionScope.user!=null}">
    <jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
    <c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
</c:if>
<html>
<head>
    <title><fmt:message key="locale.title.index" bundle="${lang}"/></title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/button.css">
    <link rel="stylesheet" type="text/css" href="css/loginform.css">
    <link rel="stylesheet" type="text/css" href="css/btn3d.css">
    <script src="js/main.js" type="text/javascript"></script>
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="libraryController?ACTION=main_page">
                        <fmt:message key="locale.info.library" bundle="${lang}"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="locale" title="Русский"
                               href="libraryController?ACTION=change_language&LANGUAGE=ru&PAGE=index.jsp">
                                <img class="locale" src="image/ru.svg">
                            </a></li>
                        <li><a class="locale" title="English"
                               href="libraryController?ACTION=change_language&LANGUAGE=en&PAGE=index.jsp">
                                <img class="locale" src="image/us.svg">
                            </a></li>
                        <c:if test="${status!='NONE'}">
                            <li><a  onmousedown="return false">
                                <span class="glyphicon glyphicon-user"></span> ${user.login}
                                </a>
                            </li>
                            <li><a href="libraryController?ACTION=logout">
                                    <span class="glyphicon glyphicon-log-out"></span>
                                    <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${status=='NONE'}">
                            <li><a href="index.jsp">
                                <span class="glyphicon glyphicon-log-in"></span>
                                <fmt:message key="locale.form.button.login" bundle="${lang}"/>
                            </a></li>
                        </c:if>
                    </ul>
                    <ul class="nav navbar-nav">
                        <c:if test="${status=='CLIENT'}">
                            <li><a href="libraryController?ACTION=USER_CART">
                                    <fmt:message key="locale.link.cart" bundle="${lang}"/>
                                </a>
                            </li>
                            <li><a href="libraryController?ACTION=ORDER_LIST">
                                    <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${status=='ADMIN'}">
                            <li><a href="libraryController?ACTION=ADMIN_ORDER_LIST">
                                    <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
<c:if test="${status=='NONE'}">
    <div class="container">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 well">
                <h3><fmt:message key="locale.info.welcome" bundle="${lang}"/></h3>
                <form name="login" action="libraryController" method="get" role="form">
                    <input type="hidden" name="ACTION" value="login">
                    <div class="form-group">
                        <label for="usrname">
                            <span class="glyphicon glyphicon-user"></span>
                            <fmt:message key="locale.form.label.login" bundle="${lang}"/>
                        </label>
                        <input name="LOGIN" type="text" class="form-control" id="usrname"
                               placeholder="<fmt:message key="locale.form.label.login" bundle="${lang}"/>">
                    </div>
                    <div class="form-group">
                        <label for="psw">
                            <span class="glyphicon glyphicon-eye-open"></span>
                            <fmt:message key="locale.form.label.password" bundle="${lang}"/></label>
                        <input name="PASSWORD" type="password" class="form-control" id="psw"
                               placeholder="<fmt:message key="locale.form.label.password" bundle="${lang}"/>">
                    </div>
                    <button type="submit" class="btn btn-success  btn3d btn-block">
                        <span class="glyphicon glyphicon-off"></span>
                        <fmt:message key="locale.form.button.login" bundle="${lang}"/>
                    </button>
                </form>
                <button type="button" class="btn btn3d btn-link"  data-toggle="modal" data-target="#modal-1">
                    <fmt:message key="locale.link.registration" bundle="${lang}"/>
                </button>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
    <c:if test="${requestScope.response!=null}">
        <div class="container">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4 alert alert-danger">
                    <fmt:message key="locale.message.${requestScope.response}" bundle="${lang}"/>
                </div>
                <div class="col-lg-4"></div>
            </div>
        </div>
    </c:if>
</c:if>
<br><br><br>
<c:if test="${status!='NONE'}">
    <div class="container">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 alert alert-success">
                <h1><fmt:message key="locale.info.welcome" bundle="${lang}"/></h1>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</c:if>

<div class="modal fade"  id="modal-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i>
                </button>
                <h4 class="modal-title"><fmt:message key="locale.form.button.registration" bundle="${lang}"/></h4>
            </div>
            <div class="modal-body">
                <form name="registration" action="libraryController" method="get" class="form-horizontal" role="form">
                    <input type="hidden" name="ACTION" value="registration">
                    <div class="form-group">
                        <label  class="control-label col-sm-2" for="login">
                            <fmt:message key="locale.form.label.login" bundle="${lang}"/>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                                <input name="LOGIN" pattern="[a-zA-Z0-9_]{5,45}" type="text"
                                       class="form-control" id="login"
                                       placeholder="<fmt:message key="locale.form.label.login" bundle="${lang}"/>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="password">
                            <fmt:message key="locale.form.label.password" bundle="${lang}"/>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon2">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </span>
                                <input name="PASSWORD" pattern="[a-zA-Z0-9_]{5,45}" type="password"
                                       class="form-control" id="password"
                                       placeholder="<fmt:message key="locale.form.label.password" bundle="${lang}"/>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="fName">
                            <fmt:message key="locale.form.label.fName" bundle="${lang}"/>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">
                                    <span class="glyphicon glyphicon-text-color"></span>
                                </span>
                                <input name="FNAME" pattern="[а-яА-яa-zA-z ]+" type="text"
                                       class="form-control" id="fName"
                                       placeholder="<fmt:message key="locale.form.label.fName" bundle="${lang}"/>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="lName">
                            <fmt:message key="locale.form.label.lName" bundle="${lang}"/>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon4">
                                    <span class="glyphicon glyphicon-text-background"></span>
                                </span>
                                <input name="LNAME" pattern="[а-яА-яa-zA-z ]+" type="text"
                                       class="form-control" id="lName"
                                       placeholder="<fmt:message key="locale.form.label.lName" bundle="${lang}"/>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone">
                            <fmt:message key="locale.form.label.number" bundle="${lang}"/>
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon5">
                                    <span class="glyphicon glyphicon-earphone"></span>
                                </span>
                                <input name="NUMBER" pattern="[0-9]{9}" type="text"
                                       class="form-control" id="phone"
                                       placeholder="<fmt:message key="locale.form.label.phoneNumber" bundle="${lang}"/>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success btn3d btn-block">
                        <span class="glyphicon glyphicon-ok"></span>
                        <fmt:message key="locale.form.button.registration" bundle="${lang}"/>
                    </button>

                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn3d btn-danger" type="button" data-dismiss="modal">
                    <fmt:message key="locale.form.button.cancel" bundle="${lang}"/>
                </button>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jq-min.js" type="text/javascript"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<hr>
<footer class="copyright">
    <i>
        <user-tag:copyright name="Nikita Kobyzov"/>
    </i>
</footer>
</body>
</html>
