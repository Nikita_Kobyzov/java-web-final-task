<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<c:set var="status" scope="page" value="NONE"/>
<c:if test="${sessionScope.user!=null}">
  <jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
  <c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
</c:if>
<jsp:useBean id="backlist" class="by.epam.task6.domain.vo.BackList" scope="request"/>
<html>
<head>
  <title><fmt:message key="locale.title.main" bundle="${lang}"/></title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.min.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.css.map" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css.map" rel="stylesheet">
  <script src="bootstrap-3.3.6-dist/js/bootstrap.js"></script>
  <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  <script src="bootstrap-3.3.6-dist/js/npm.js"></script>
  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/jquery.js"></script>
  <script src="js/jq-1.1.3.js" type="text/javascript"></script>
  <script src="js/jq-2.1.4.js" type="text/javascript"></script>
  <script src="js/jq-min.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
</head>
<body onload="setFirstPage()">
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&LAST_COMMAND=main_page">
                  <img class="locale" src="image/ru.svg">
                </a>
            </li>
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=en&LAST_COMMAND=main_page">
                  <img class="locale" src="image/us.svg">
                </a>
            </li>
            <c:if test="${status!='NONE'}">
              <li><a  onmousedown="return false"><span class="glyphicon glyphicon-user"></span> ${user.login}</a></li>
              <li><a href="libraryController?ACTION=logout">
                    <span class="glyphicon glyphicon-log-out"></span>
                    <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
                  </a>
              </li>
            </c:if>
            <c:if test="${status=='NONE'}">
              <li><a href="index.jsp">
                  <span class="glyphicon glyphicon-log-in"></span>
                  <fmt:message key="locale.form.button.login" bundle="${lang}"/>
                </a>
              </li>
            </c:if>
          </ul>
          <ul class="nav navbar-nav">
            <c:if test="${status=='CLIENT'}">
              <li><a href="libraryController?ACTION=USER_CART">
                    <fmt:message key="locale.link.cart" bundle="${lang}"/>
                  </a>
              </li>
              <li><a href="libraryController?ACTION=ORDER_LIST">
                    <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
                  </a>
              </li>
            </c:if>
            <c:if test="${status=='ADMIN'}">
              <li><a href="libraryController?ACTION=ADMIN_ORDER_LIST">
                    <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
                  </a>
              </li>
            </c:if>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br>
<div class="container">
  <div class="col-lg-2">
    <ul class="nav nav-list sidebar">
      <li class="nav-header"><h2><fmt:message key="locale.info.genres" bundle="${lang}"/></h2></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=warfare">
        <fmt:message key="locale.info.genre.warfare" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=detective">
        <fmt:message key="locale.info.genre.detective" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=childish">
        <fmt:message key="locale.info.genre.childish" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=dramaturgy">
        <fmt:message key="locale.info.genre.dramaturgy" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=computers_internet">
        <fmt:message key="locale.info.genre.computers_internet" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=thriller">
        <fmt:message key="locale.info.genre.thriller" bundle="${lang}"/>
      </a></li>
      <li><a href="libraryController?ACTION=SELECT_ON_GENRE&genre=fantasy">
        <fmt:message key="locale.info.genre.fantasy" bundle="${lang}"/>
      </a></li>
    </ul>
  </div>
  <div class="col-lg-10">
    <c:if test="${status=='ADMIN'}">
      <button class="btn btn3d btn-lg btn-success"  data-toggle="modal" data-target="#modal-1">
        <span class="glyphicon glyphicon-plus"></span>
        <fmt:message key="locale.form.button.add_book" bundle="${lang}"/>
      </button>
      <br><br>
    </c:if>
    <div class="row">
      <ul class="pagination">
        <c:forEach items="${backlist.pageList}" var="page">
          <li><a id="page-${page.id}" onclick="return up()" href="#tab-${page.id}" data-toggle="tab">${page.id}</a></li>
        </c:forEach>
      </ul>
    </div>
    <div class="row">
      <div class="tabs">
        <div class="tab-content">
          <c:forEach items="${backlist.pageList}" var="page">
            <div class="tab-pane fade" id="tab-${page.id}">
              <c:forEach items="${page.bookRecordVOList}" var="book_record">
                  <div class="col-xs-2 col-sm-3">
                    <div class="thumbnail element">
                      <a class="thumbnail image-link" href="libraryController?ACTION=BOOK_VIEW&ID=${book_record.book.id}">
                        <img class="title-img" src="image/book/${book_record.book.image}" alt="Book Image">
                      </a>
                      <div class="caption footer">
                        <span><a href="libraryController?ACTION=BOOK_VIEW&ID=${book_record.book.id}">
                          ${book_record.book.title}
                        </a></span>
                        <p>${book_record.book.author}</p>
                        <div class="btn-group">
                          <a href="libraryController?ACTION=BOOK_VIEW&ID=${book_record.book.id}" class="btn btn-success">
                            <fmt:message key="locale.link.read_more" bundle="${lang}"/>
                            <span class="glyphicon glyphicon-hand-up"></span>
                          </a>
                          <c:if test="${status=='ADMIN'}">
                            <a class="btn btn-success dropdown" data-toggle="dropdown">
                              \<span class="caret"></span>/
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn btn-link" onclick="setBookID(${book_record.book.id})"
                                     data-toggle="modal" data-target="#modal-2">
                                <fmt:message key="locale.form.button.delete" bundle="${lang}"/>
                              </a></li>
                              <li><a class="btn btn-link" href="libraryController?ACTION=BOOK_EDITOR&ID=${book_record.id}">
                                <fmt:message key="locale.form.button.edit" bundle="${lang}"/>
                              </a></li>
                            </ul>
                          </c:if>
                        </div>
                      </div>
                    </div>
                  </div>
              </c:forEach>
            </div>
          </c:forEach>
        </div>
      </div>
    </div>

    <hr>
    <footer class="copyright">
      <i><user-tag:copyright name="Nikita Kobyzov"/></i>
    </footer>

  </div>
</div>
<div class="modal fade"  id="modal-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" type="button" data-dismiss="modal">
          <i class="glyphicon glyphicon-remove"></i>
        </button>
        <h4 class="modal-title"><fmt:message key="locale.form.button.add_book" bundle="${lang}"/></h4>
      </div>
      <div class="modal-body">
        <form action="libraryController" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
          <input type="hidden" name="ACTION" value="ADD_BOOK">
          <div class="form-group">
            <label  class="control-label col-sm-2" for="title">
              <fmt:message key="locale.info.book.title" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                  <span class="glyphicon glyphicon-font"></span>
                </span>
                <input name="TITLE" pattern="[a-zA-ZА-Яа-я0-9_.,' ]{1,45}" type="text" class="form-control" id="title">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="author">
              <fmt:message key="locale.info.book.author" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon2">
                  <span class="glyphicon glyphicon-user"></span>
                </span>
                <input name="AUTHOR" pattern="[a-zA-ZА-Яа-я ]{2,45}" type="text" class="form-control" id="author">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="size">
              <fmt:message key="locale.info.book.size" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon3">
                  <span class="glyphicon glyphicon-book"></span>
                </span>
                <input name="SIZE" pattern="[0-9]{1,45}" type="text" class="form-control" id="size">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="genre">
              <fmt:message key="locale.info.book.genre" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">
                  <span class="glyphicon glyphicon-star-empty"></span>
                </span>
                <select name="GENRE" class="form-control" id="genre">
                  <option value="warfare">
                    <fmt:message key="locale.info.genre.warfare" bundle="${lang}"/>
                  </option>
                  <option value="detective">
                    <fmt:message key="locale.info.genre.detective" bundle="${lang}"/>
                  </option>
                  <option value="childish">
                    <fmt:message key="locale.info.genre.childish" bundle="${lang}"/>
                  </option>
                  <option value="dramaturgy">
                    <fmt:message key="locale.info.genre.dramaturgy" bundle="${lang}"/>
                  </option>
                  <option value="computers_internet">
                    <fmt:message key="locale.info.genre.computers_internet" bundle="${lang}"/>
                  </option>
                  <option value="thriller">
                    <fmt:message key="locale.info.genre.thriller" bundle="${lang}"/>
                  </option>
                  <option value="fantasy">
                    <fmt:message key="locale.info.genre.fantasy" bundle="${lang}"/>
                  </option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="image">
              <fmt:message key="locale.info.book.image" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary btn-file">
                        Browse&hellip; <input name="IMAGE" id="image" accept="image/*" type="file">
                    </span>
                </span>
                <input type="text" id="text" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="annotation">
              <fmt:message key="locale.info.book.annotation" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon6">
                  <span class="glyphicon glyphicon-pencil"></span>
                </span>
                <textarea name="ANNOTATION" pattern="[a-zA-ZА-Яа-я0-9_.,-:().!?%*/ ]{1,}"
                          class="form-control" rows="5" id="annotation"></textarea>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label  class="control-label col-sm-2" for="count">
              <fmt:message key="locale.info.book.count" bundle="${lang}"/>
            </label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon7">
                  <span class="glyphicon glyphicon-option-horizontal"></span>
                </span>
                <input name="FREE_COUNT" pattern="[0-9]{1,45}" type="text"
                       class="form-control" id="count">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-success btn3d btn-block">
            <span class="glyphicon glyphicon-ok"></span>
            <fmt:message key="locale.form.button.add_book" bundle="${lang}"/>
          </button>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn3d btn-danger" type="button" data-dismiss="modal">
          <fmt:message key="locale.form.button.cancel" bundle="${lang}"/>
        </button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade"  id="modal-2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h3><fmt:message key="locale.message.delete_question" bundle="${lang}"/></h3>
        <button class="btn btn-primary" data-dismiss="modal">
          <fmt:message key="locale.message.no" bundle="${lang}"/>
        </button>
       <a class="btn btn-primary" id="bookID"><fmt:message key="locale.message.yes" bundle="${lang}"/></a>
      </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jq-min.js" type="text/javascript"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>
