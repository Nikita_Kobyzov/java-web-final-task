<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${sessionScope.lang}" scope="request"/>
<fmt:setBundle basename="locale" var="lang"/>
<jsp:useBean id="user" class="by.epam.task6.domain.User" scope="session"/>
<c:set var="status" scope="page" value="${user.userStatus.toString()}"/>
<jsp:useBean id="book_record" class="by.epam.task6.domain.vo.BookRecordVO" scope="request"/>
<html>
<head>
  <title><fmt:message key="locale.title.edit_book" bundle="${lang}"/></title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/button.css">
  <link rel="stylesheet" type="text/css" href="css/btn3d.css">
  <link rel="stylesheet" type="text/css" href="css/loginform.css">
  <link href="bootstrap-3.3.6-dist/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="bootstrap-3.3.6-dist/js/bootstrap.js"></script>
  <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  <script src="bootstrap-3.3.6-dist/js/npm.js"></script>
  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/jquery.js"></script>
  <script src="js/jq-1.1.3.js" type="text/javascript"></script>
  <script src="js/jq-2.1.4.js" type="text/javascript"></script>
  <script src="js/jq-min.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="libraryController?ACTION=main_page">
            <fmt:message key="locale.info.library" bundle="${lang}"/>
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=ru&LAST_COMMAND=BOOK_EDITOR&ID=${book_record.id}">
              <img class="locale" src="image/ru.svg">
            </a></li>
            <li><a class="locale"
                   href="libraryController?ACTION=change_language&LANGUAGE=en&LAST_COMMAND=BOOK_EDITOR&ID=${book_record.id}">
              <img class="locale" src="image/us.svg">
            </a></li>
            <li><a onmousedown="return false"><span class="glyphicon glyphicon-user"></span> ${user.login}</a></li>
            <li><a href="libraryController?ACTION=logout">
              <span class="glyphicon glyphicon-log-out"></span>
              <fmt:message key="locale.form.button.logout" bundle="${lang}"/>
            </a></li>
          </ul>
          <ul class="nav navbar-nav">
            <li><a href="libraryController?ACTION=ADMIN_ORDER_LIST">
              <fmt:message key="locale.link.taken_books" bundle="${lang}"/>
            </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br><br><br>
<div class="row">
  <div class="col-lg-3"></div>
  <div class="col-lg-6 well">
      <form action="libraryController" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
        <input type="hidden" name="ACTION" value="EDIT_BOOK">
        <input type="hidden" name="ID" value="${book_record.book.id}">
        <input type="hidden" name="LAST_IMAGE" value="${book_record.book.image}">
        <div class="form-group">
          <label  class="control-label col-sm-2" for="title">
            <fmt:message key="locale.info.book.title" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">
                <span class="glyphicon glyphicon-font"></span>
              </span>
              <input name="TITLE" pattern="[a-zA-ZА-Яа-я0-9_ ]{1,45}" type="text"
                     class="form-control" id="title" value="${book_record.book.title}">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="author">
            <fmt:message key="locale.info.book.author" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon2">
                <span class="glyphicon glyphicon-user"></span>
              </span>
              <input name="AUTHOR" pattern="[a-zA-ZА-Яа-я ]{2,45}" type="text"
                     class="form-control" id="author" value="${book_record.book.author}">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="size">
            <fmt:message key="locale.info.book.size" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon3">
                <span class="glyphicon glyphicon-book"></span>
              </span>
              <input name="SIZE" pattern="[0-9]{1,45}" type="text"
                     class="form-control" id="size" value="${book_record.book.size}">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="genre">
            <fmt:message key="locale.info.book.genre" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon4">
                <span class="glyphicon glyphicon-star-empty"></span>
              </span>
              <select name="GENRE" class="form-control" id="genre">
                <option <c:if test="${book_record.book.genre.equals('warfare')}">selected</c:if> value="warfare">
                  <fmt:message key="locale.info.genre.warfare" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('detective')}">selected</c:if> value="detective">
                  <fmt:message key="locale.info.genre.detective" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('childish')}">selected</c:if> value="childish">
                  <fmt:message key="locale.info.genre.childish" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('dramaturgy')}">selected</c:if> value="dramaturgy">
                  <fmt:message key="locale.info.genre.dramaturgy" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('computers_internet')}">selected</c:if>
                        value="computers_internet">
                  <fmt:message key="locale.info.genre.computers_internet" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('thriller')}">selected</c:if> value="thriller">
                  <fmt:message key="locale.info.genre.thriller" bundle="${lang}"/>
                </option>
                <option <c:if test="${book_record.book.genre.equals('fantasy')}">selected</c:if> value="fantasy">
                  <fmt:message key="locale.info.genre.fantasy" bundle="${lang}"/>
                </option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="image">
            <fmt:message key="locale.info.book.image" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary btn-file">
                      <fmt:message key="locale.link.browse" bundle="${lang}"/>&hellip;
                      <input name="IMAGE" id="image" accept="image/*" type="file">
                    </span>
                </span>
              <input type="text" name="IMAGE" id="text"
                     class="form-control" readonly>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="annotation">
            <fmt:message key="locale.info.book.annotation" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon6">
                <span class="glyphicon glyphicon-pencil"></span>
              </span>
              <textarea name="ANNOTATION" pattern="[a-zA-ZА-Яа-я0-9_.,-:().!?%*/ ]{1,}"
                        class="form-control" rows="5" id="annotation">${book_record.book.annotation}
              </textarea>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label  class="control-label col-sm-2" for="count">
            <fmt:message key="locale.info.book.count" bundle="${lang}"/>
          </label>
          <div class="col-sm-10">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon7">
                <span class="glyphicon glyphicon-option-horizontal"></span>
              </span>
              <input name="FREE_COUNT" pattern="[0-9]{1,45}" type="text"
                     class="form-control" id="count" value="${book_record.freeCount}">
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-success btn3d btn-block">
          <span class="glyphicon glyphicon-ok"></span>
          <fmt:message key="locale.form.button.edit" bundle="${lang}"/>
        </button>
      </form>
    </div>
  </div>
<hr>
<footer class="copyright">
  <i>
    <user-tag:copyright name="Nikita Kobyzov"/>
  </i>
</footer>
</body>
</html>
